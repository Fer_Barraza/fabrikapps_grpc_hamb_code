-- MySQL dump 10.13  Distrib 8.0.23, for Linux (x86_64)
--
-- Host: localhost    Database: payment_checkout_db4
-- ------------------------------------------------------
-- Server version	8.0.23-0ubuntu0.20.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Address`
--

DROP TABLE IF EXISTS `Address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Address` (
  `Address_Id` int unsigned NOT NULL AUTO_INCREMENT,
  `Address_PayerId` bigint unsigned NOT NULL,
  `Address_Street_Name` varchar(45) NOT NULL,
  `Address_Street_Number` int NOT NULL,
  PRIMARY KEY (`Address_Id`),
  KEY `FK_Address_Payer_idx` (`Address_PayerId`),
  CONSTRAINT `FK_Address_Payer` FOREIGN KEY (`Address_PayerId`) REFERENCES `Payer` (`Payer_Id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Address`
--

LOCK TABLES `Address` WRITE;
/*!40000 ALTER TABLE `Address` DISABLE KEYS */;
INSERT INTO `Address` VALUES (35,54855645,'SUAREZ',6644),(36,1155647,'LEJOS',355);
/*!40000 ALTER TABLE `Address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Branch`
--

DROP TABLE IF EXISTS `Branch`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Branch` (
  `Branch_Id` mediumint unsigned NOT NULL AUTO_INCREMENT,
  `Branch_Name` varchar(45) DEFAULT NULL,
  `Branch_Headquarters_Id` mediumint unsigned NOT NULL,
  PRIMARY KEY (`Branch_Id`),
  KEY `Branch_FK` (`Branch_Headquarters_Id`),
  CONSTRAINT `Branch_FK` FOREIGN KEY (`Branch_Headquarters_Id`) REFERENCES `Headquarters` (`Headquarters_Id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Branch`
--

LOCK TABLES `Branch` WRITE;
/*!40000 ALTER TABLE `Branch` DISABLE KEYS */;
INSERT INTO `Branch` VALUES (1,'Fabrikapps',1);
/*!40000 ALTER TABLE `Branch` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Headquarters`
--

DROP TABLE IF EXISTS `Headquarters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Headquarters` (
  `Headquarters_Id` mediumint unsigned NOT NULL AUTO_INCREMENT,
  `Headquarters_Name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`Headquarters_Id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Headquarters`
--

LOCK TABLES `Headquarters` WRITE;
/*!40000 ALTER TABLE `Headquarters` DISABLE KEYS */;
INSERT INTO `Headquarters` VALUES (1,'Sede MG'),(2,'Sede SJ');
/*!40000 ALTER TABLE `Headquarters` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Item`
--

DROP TABLE IF EXISTS `Item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Item` (
  `Item_Id` int unsigned NOT NULL AUTO_INCREMENT,
  `Item_Category_Id` int unsigned NOT NULL,
  `Item_Title` varchar(45) NOT NULL,
  `Item_Currency_id` varchar(5) DEFAULT 'ARS',
  `Item_Unit_Price` float NOT NULL,
  PRIMARY KEY (`Item_Id`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Item`
--

LOCK TABLES `Item` WRITE;
/*!40000 ALTER TABLE `Item` DISABLE KEYS */;
INSERT INTO `Item` VALUES (1,1,'Matadora','ARS',250),(2,1,'Poderosa','ARS',300),(100,2,'La Combinada','ARS',120);
/*!40000 ALTER TABLE `Item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Item_has_Order_Detail`
--

DROP TABLE IF EXISTS `Item_has_Order_Detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Item_has_Order_Detail` (
  `Item_has_Order_Detail` bigint unsigned NOT NULL AUTO_INCREMENT,
  `Item_Item_Id` int unsigned NOT NULL,
  `Order_Order_Id` bigint unsigned NOT NULL,
  `Item_has_Order_Detail_Quantity` mediumint unsigned NOT NULL,
  `Item_has_Order_Detail_Unit_Price` float NOT NULL,
  PRIMARY KEY (`Item_has_Order_Detail`),
  KEY `fk_Item_has_Order_Item1_idx` (`Item_Item_Id`),
  KEY `fk_Item_has_Order_Order1_idx` (`Order_Order_Id`),
  CONSTRAINT `fk_Item_has_Order_Item1` FOREIGN KEY (`Item_Item_Id`) REFERENCES `Item` (`Item_Id`) ON UPDATE CASCADE,
  CONSTRAINT `fk_Item_has_Order_Order1` FOREIGN KEY (`Order_Order_Id`) REFERENCES `Orders` (`Order_Id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Item_has_Order_Detail`
--

LOCK TABLES `Item_has_Order_Detail` WRITE;
/*!40000 ALTER TABLE `Item_has_Order_Detail` DISABLE KEYS */;
INSERT INTO `Item_has_Order_Detail` VALUES (60,1,36,1,250),(61,2,36,3,300),(62,1,37,2,250),(63,2,37,3,300);
/*!40000 ALTER TABLE `Item_has_Order_Detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Order_Status`
--

DROP TABLE IF EXISTS `Order_Status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Order_Status` (
  `Order_Status_Id` smallint NOT NULL AUTO_INCREMENT,
  `Order_Status_Type` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`Order_Status_Id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Order_Status`
--

LOCK TABLES `Order_Status` WRITE;
/*!40000 ALTER TABLE `Order_Status` DISABLE KEYS */;
INSERT INTO `Order_Status` VALUES (1,'--'),(2,'En proceso'),(3,'Entregado'),(4,'Cancelado'),(5,'En devolución');
/*!40000 ALTER TABLE `Order_Status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Orders`
--

DROP TABLE IF EXISTS `Orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Orders` (
  `Order_Id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `Order_PayerId` bigint unsigned NOT NULL,
  `Order_HeadquarterId` mediumint unsigned NOT NULL DEFAULT '1',
  `Order_BranchId` mediumint unsigned NOT NULL,
  `Order_Status_Order_Status_Id` smallint NOT NULL DEFAULT '2',
  `Order_Statements_Desc` varchar(45) DEFAULT NULL,
  `Order_Additional_Info` varchar(45) DEFAULT NULL,
  `Order_Method_Of_Delivery` varchar(25) NOT NULL,
  `Order_Payment_Method_Opts` varchar(45) NOT NULL,
  `Order_Payment_Total_Price` float unsigned NOT NULL,
  `Order_Date` date DEFAULT NULL,
  `Order_Time` time DEFAULT NULL,
  `Order_PreferenceId` varchar(70) DEFAULT NULL,
  PRIMARY KEY (`Order_Id`),
  KEY `fk_Order_Payer1_idx` (`Order_PayerId`),
  KEY `fk_Order_Headquarters_idx` (`Order_HeadquarterId`),
  KEY `fk_Order_Status_idx` (`Order_Status_Order_Status_Id`),
  KEY `Orders_FK` (`Order_BranchId`),
  CONSTRAINT `fk_Order_Headquarters` FOREIGN KEY (`Order_HeadquarterId`) REFERENCES `Headquarters` (`Headquarters_Id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `fk_Order_Payer` FOREIGN KEY (`Order_PayerId`) REFERENCES `Payer` (`Payer_Id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `fk_Order_Status` FOREIGN KEY (`Order_Status_Order_Status_Id`) REFERENCES `Order_Status` (`Order_Status_Id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `Orders_FK` FOREIGN KEY (`Order_BranchId`) REFERENCES `Branch` (`Branch_Id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Orders`
--

LOCK TABLES `Orders` WRITE;
/*!40000 ALTER TABLE `Orders` DISABLE KEYS */;
INSERT INTO `Orders` VALUES (36,54855645,2,1,4,'Fabrikapps - Pago','Fabrikapps','Delivery','Tarjeta',550,'2021-01-04','16:52:50',NULL),(37,1155647,1,1,2,'Fabrikapps - Pago','Fabrikapps','Retiro en sucursal','Efectivo',1400,'2021-01-04','16:55:25',NULL);
/*!40000 ALTER TABLE `Orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Payer`
--

DROP TABLE IF EXISTS `Payer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Payer` (
  `Payer_Id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `Pay_Email` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `Pay_Name` varchar(30) DEFAULT NULL,
  `Pay_Surname` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`Payer_Id`)
) ENGINE=InnoDB AUTO_INCREMENT=11435644964426246 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Payer`
--

LOCK TABLES `Payer` WRITE;
/*!40000 ALTER TABLE `Payer` DISABLE KEYS */;
INSERT INTO `Payer` VALUES (1155647,'sad@gmail.com','Juan','Doe'),(54855645,'queonda@gmail.com','Fer','Bar');
/*!40000 ALTER TABLE `Payer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Phone`
--

DROP TABLE IF EXISTS `Phone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Phone` (
  `Phone_Id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `Phone_PayerId` bigint unsigned NOT NULL,
  `Phone_Area_Code` varchar(10) DEFAULT '+54 9 11',
  `Phone_Phone_Number` varchar(20) NOT NULL,
  PRIMARY KEY (`Phone_Id`),
  UNIQUE KEY `Phone_Phone_Number_UNIQUE` (`Phone_Phone_Number`),
  KEY `FK_Phone_Payer_idx` (`Phone_PayerId`),
  CONSTRAINT `FK_Phone_Payer` FOREIGN KEY (`Phone_PayerId`) REFERENCES `Payer` (`Payer_Id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Phone`
--

LOCK TABLES `Phone` WRITE;
/*!40000 ALTER TABLE `Phone` DISABLE KEYS */;
INSERT INTO `Phone` VALUES (35,54855645,'+54 9 11','54855645'),(36,1155647,'+54 9 11','1155647');
/*!40000 ALTER TABLE `Phone` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-02-02 20:06:53
