package database

import (
	"context"
)

// Database abstraction
type Database interface {
	Set(ctx context.Context, key string, value string) (string, error)
	Get(ctx context.Context, key string) (string, error)
	GetPlatformKeys(ctx context.Context) (*PlatformKeys, error)
	GetPlatformHashMap(ctx context.Context, hashMapKey string) (*PlatformKeys, error)
}

// Factory looks up acording to the databaseName the database implementation
func Factory(databaseName string, passdb string) (Database, error) {
	switch databaseName {
	case "enviromentredisgrpc":
		return createRedisDatabase(databaseName, passdb)
	case "localhost":
		return createRedisDatabase(databaseName, passdb)
	default:
		return nil, &NotImplementedDatabaseError{databaseName}
	}
}

type PlatformKeys struct {
	AppTestNeedsUpdate          string `redis:"appTestNeedsUpdate"`
	AppTestWorking              string `redis:"appTestWorking"`
	AppTestDeliveryPrice        string `redis:"appTestDeliveryPrice"`
	TestPublicKeyMP             string `redis:"testPublicKeyMP"`
	TestFabrikappsNumberPhoneWP string `redis:"testFabrikappsNumberPhoneWP"`
}
