package database

import (
	"context"
	"fmt"

	redis "github.com/go-redis/redis/v8"
)

type redisDatabase struct {
	client *redis.Client
}

var platformKeys = []string{"appTestNeedsUpdate", "appTestWorking", "appTestDeliveryPrice", "testPublicKeyMP", "testFabrikappsNumberPhoneWP"}

// CreateRedisDatabase creates the redis database
func createRedisDatabase(databaseName string, passdb string) (Database, error) {
	address := fmt.Sprintf("%s:6379", databaseName)
	client := redis.NewClient(&redis.Options{
		Addr:     address,
		Password: passdb, // no password set
		DB:       0,      // use default DB
	})
	_, err := client.Ping(context.Background()).Result() // makes sure database is connected
	if err != nil {
		return nil, &CreateDatabaseError{}
	}
	return &redisDatabase{client: client}, nil
}

func (r *redisDatabase) Set(ctx context.Context, key string, value string) (string, error) {
	_, err := r.client.Set(context.Background(), key, value, 0).Result()
	if err != nil {
		return generateError("set", err)
	}
	return key, nil
}

func (r *redisDatabase) Get(ctx context.Context, key string) (string, error) {
	value, err := r.client.Get(context.Background(), key).Result()
	if err != nil {
		return generateError("get", err)
	}
	return value, nil

}
func (r *redisDatabase) GetPlatformKeys(ctx context.Context) (*PlatformKeys, error) {
	var pK PlatformKeys
	value := r.client.MGet(ctx, platformKeys...)
	print(value)
	// if err != nil {
	// 	return generateError("get", err)
	// }
	err := value.Scan(&pK)
	if err != nil {
		var _, err = generateError("getPlatformKeys", err)
		return &pK, err
	}
	return &pK, nil

}
func (r *redisDatabase) GetPlatformHashMap(ctx context.Context, hashMapKey string) (*PlatformKeys, error) {
	// var pK PlatformKeys
	hashMapResponse := PlatformKeys{}

	value := r.client.HGetAll(ctx, hashMapKey)
	err := value.Scan(&hashMapResponse)

	// redis.ScanStruct(asdf, &hashMapResponse)

	// value := r.client.MGet(ctx, platformKeys...)
	// print(value)
	// if err != nil {
	// 	return generateError("get", err)
	// }
	// err := value.Scan(&pK)
	if err != nil {
		var _, err = generateError("getPlatformKeys", err)
		return &hashMapResponse, err
	}
	return &hashMapResponse, nil

}

func generateError(operation string, err error) (string, error) {
	if err == redis.Nil {
		return "", &OperationError{operation}
	}
	return "", &DownError{}
}
