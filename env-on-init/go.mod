module fabrikapps/env-on-init

go 1.15

require (
	github.com/go-redis/redis/v8 v8.8.2
	google.golang.org/grpc v1.37.0
	google.golang.org/protobuf v1.25.0
)
