package main

import (
	"context"
	"fabrikapps/env-on-init/database"
	proto "fabrikapps/env-on-init/proto/protopb"
	"fmt"
	"net"

	"os"

	"google.golang.org/grpc"
)

type server struct{}

var db database.Database

func main() {
	listener, err := net.Listen("tcp", ":4060")
	if err != nil {
		panic(err) // The port may be on use
	}
	srv := grpc.NewServer()
	var passdb = os.Getenv("REDIS_PASS")
	databaseImplementation := os.Args[1]
	// var passdb = "DE05MEBnhwhp2go4+qff46UNSw1wPnekn+cO1+YyG9RbxoXcRv7mv3cbPuMyZV5xvWzZUhCP5hOGBNKP"
	// databaseImplementation := "localhost"
	db, err = database.Factory(databaseImplementation, passdb)
	if err != nil {
		panic(err)
	}
	proto.RegisterBasicServiceServer(srv, &server{})
	fmt.Println("Prepare to serve")
	if e := srv.Serve(listener); e != nil {
		panic(err)
	}
}
func (s *server) Set(ctx context.Context, in *proto.SetRequest) (*proto.ServerResponse, error) {
	value, err := db.Set(ctx, in.GetKey(), in.GetValue())
	return generateResponse(value, err)
}
func (s *server) Get(ctx context.Context, in *proto.GetRequest) (*proto.ServerResponse, error) {
	value, err := db.Get(ctx, in.GetKey())
	return generateResponse(value, err)
}

// func (s *server) GetPlatformKeys(ctx context.Context, in *proto.GetPlatformKeysRequest) (*proto.PlatformKeysResponse, error) {
// 	platformValues, err := db.GetPlatformKeys(ctx)
// 	var platformKeysResponse *proto.PlatformKeys
// 	// platformKeysResponse = &proto.PlatformKeys{} //entrega struct vacío
// 	// return generateResponse(value, err)
// 	if err != nil {
// 		return &proto.PlatformKeysResponse{Success: false, PlatformValues: platformKeysResponse, Error: err.Error()}, nil
// 	}

// 	platformKeysResponse = &proto.PlatformKeys{
// 		AppTestDeliveryPrice:        platformValues.AppTestDeliveryPrice,
// 		AppTestNeedsUpdate:          platformValues.AppTestNeedsUpdate,
// 		AppTestWorking:              platformValues.AppTestWorking,
// 		TestFabrikappsNumberPhoneWP: platformValues.TestFabrikappsNumberPhoneWP,
// 		TestPublicKeyMP:             platformValues.TestPublicKeyMP,
// 	}

// 	return &proto.PlatformKeysResponse{
// 		Success:        true,
// 		PlatformValues: platformKeysResponse,
// 		Error:          "",
// 	}, nil
// }
func (s *server) GetPlatformHashMap(ctx context.Context, in *proto.GetPlatformKeysRequest) (*proto.PlatformKeysResponse, error) {
	platformValues, err := db.GetPlatformHashMap(ctx, in.Hashmapkey)
	var platformKeysResponse *proto.PlatformKeys
	// platformKeysResponse = &proto.PlatformKeys{} //entrega struct vacío
	// return generateResponse(value, err)
	if err != nil {
		return &proto.PlatformKeysResponse{Success: false, PlatformValues: platformKeysResponse, Error: err.Error()}, nil
	}

	platformKeysResponse = &proto.PlatformKeys{
		AppTestDeliveryPrice:        platformValues.AppTestDeliveryPrice,
		AppTestNeedsUpdate:          platformValues.AppTestNeedsUpdate,
		AppTestWorking:              platformValues.AppTestWorking,
		TestFabrikappsNumberPhoneWP: platformValues.TestFabrikappsNumberPhoneWP,
		TestPublicKeyMP:             platformValues.TestPublicKeyMP,
	}

	return &proto.PlatformKeysResponse{
		Success:        true,
		PlatformValues: platformKeysResponse,
		Error:          "",
	}, nil
}

func generateResponse(value string, err error) (*proto.ServerResponse, error) {
	if err != nil {
		return &proto.ServerResponse{Success: false, Value: value, Error: err.Error()}, nil
	}
	return &proto.ServerResponse{Success: true, Value: value, Error: ""}, nil
}
