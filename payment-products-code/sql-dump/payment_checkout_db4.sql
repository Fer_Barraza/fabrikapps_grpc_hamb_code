-- MySQL dump 10.13  Distrib 8.0.23, for Linux (x86_64)
--
-- Host: localhost    Database: payment_checkout_db4
-- ------------------------------------------------------
-- Server version	8.0.23-0ubuntu0.20.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Branch`
--

DROP TABLE IF EXISTS `Branch`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Branch` (
  `Branch_Id` mediumint unsigned NOT NULL AUTO_INCREMENT,
  `Branch_Name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`Branch_Id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Branch`
--

LOCK TABLES `Branch` WRITE;
/*!40000 ALTER TABLE `Branch` DISABLE KEYS */;
INSERT INTO `Branch` VALUES (1,'Fabrikapps'),(2,'ECorp');
/*!40000 ALTER TABLE `Branch` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Branch_has_Headquarters`
--

DROP TABLE IF EXISTS `Branch_has_Headquarters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Branch_has_Headquarters` (
  `Branch_Branch_Id` mediumint unsigned NOT NULL,
  `Headquarters_Headquarters_Id` mediumint unsigned NOT NULL,
  PRIMARY KEY (`Branch_Branch_Id`,`Headquarters_Headquarters_Id`),
  KEY `fk_Branch_has_Headquarters_Headquarters1_idx` (`Headquarters_Headquarters_Id`),
  KEY `fk_Branch_has_Headquarters_Branch1_idx` (`Branch_Branch_Id`),
  CONSTRAINT `fk_Branch_has_Headquarters_Branch1` FOREIGN KEY (`Branch_Branch_Id`) REFERENCES `Branch` (`Branch_Id`),
  CONSTRAINT `fk_Branch_has_Headquarters_Headquarters1` FOREIGN KEY (`Headquarters_Headquarters_Id`) REFERENCES `Headquarters` (`Headquarters_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Branch_has_Headquarters`
--

LOCK TABLES `Branch_has_Headquarters` WRITE;
/*!40000 ALTER TABLE `Branch_has_Headquarters` DISABLE KEYS */;
INSERT INTO `Branch_has_Headquarters` VALUES (1,1),(1,2),(2,3);
/*!40000 ALTER TABLE `Branch_has_Headquarters` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Category`
--

DROP TABLE IF EXISTS `Category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Category` (
  `CategoriasMenu_id` int unsigned NOT NULL AUTO_INCREMENT,
  `CategoriasMenuNombre` varchar(25) NOT NULL,
  PRIMARY KEY (`CategoriasMenu_id`),
  UNIQUE KEY `CategoriasMenuNombre_UNIQUE` (`CategoriasMenuNombre`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Category`
--

LOCK TABLES `Category` WRITE;
/*!40000 ALTER TABLE `Category` DISABLE KEYS */;
/*!40000 ALTER TABLE `Category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Headquarters`
--

DROP TABLE IF EXISTS `Headquarters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Headquarters` (
  `Headquarters_Id` mediumint unsigned NOT NULL AUTO_INCREMENT,
  `Headquarters_Name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`Headquarters_Id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Headquarters`
--

LOCK TABLES `Headquarters` WRITE;
/*!40000 ALTER TABLE `Headquarters` DISABLE KEYS */;
INSERT INTO `Headquarters` VALUES (1,'Sede MG'),(2,'Sede SJ'),(3,'Sede New York');
/*!40000 ALTER TABLE `Headquarters` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Item`
--

DROP TABLE IF EXISTS `Item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Item` (
  `Item_Id` bigint unsigned NOT NULL,
  `Item_Category_Id` int unsigned NOT NULL,
  `Item_Title` varchar(45) NOT NULL,
  `Item_Currency_id` varchar(5) DEFAULT 'ARS',
  `Item_Unit_Price` float NOT NULL,
  `Item_CategoriasMenu_id` int unsigned NOT NULL,
  `Item_Description` varchar(45) DEFAULT NULL,
  `Item_ImageName` varchar(90) DEFAULT NULL,
  `Item_Branch_Id` mediumint unsigned NOT NULL,
  PRIMARY KEY (`Item_Id`),
  KEY `fk_Item_Category1_idx` (`Item_CategoriasMenu_id`),
  CONSTRAINT `fk_Item_Category1` FOREIGN KEY (`Item_CategoriasMenu_id`) REFERENCES `Category` (`CategoriasMenu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Item`
--

LOCK TABLES `Item` WRITE;
/*!40000 ALTER TABLE `Item` DISABLE KEYS */;
INSERT INTO `Item` VALUES (1,1,'Matadora','ARS',250,0,NULL,NULL,0),(2,1,'Poderosa','ARS',300,0,NULL,NULL,0),(100,2,'La Combinada','ARS',120,0,NULL,NULL,0);
/*!40000 ALTER TABLE `Item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Item_has_Order_Detail`
--

DROP TABLE IF EXISTS `Item_has_Order_Detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Item_has_Order_Detail` (
  `Item_has_Order_Detail` bigint unsigned NOT NULL AUTO_INCREMENT,
  `Item_Item_Id` bigint unsigned NOT NULL,
  `Order_Order_Id` bigint unsigned NOT NULL,
  `Item_has_Order_Detail_Quantity` mediumint unsigned NOT NULL,
  `Item_has_Order_Detail_Unit_Price` float NOT NULL,
  PRIMARY KEY (`Item_has_Order_Detail`),
  KEY `fk_Item_has_Order_Item1_idx` (`Item_Item_Id`),
  KEY `fk_Item_has_Order_Order1_idx` (`Order_Order_Id`),
  CONSTRAINT `fk_Item_has_Order_Item1` FOREIGN KEY (`Item_Item_Id`) REFERENCES `Item` (`Item_Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_Item_has_Order_Order1` FOREIGN KEY (`Order_Order_Id`) REFERENCES `Orders` (`Order_Id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=238 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Item_has_Order_Detail`
--

LOCK TABLES `Item_has_Order_Detail` WRITE;
/*!40000 ALTER TABLE `Item_has_Order_Detail` DISABLE KEYS */;
INSERT INTO `Item_has_Order_Detail` VALUES (60,1,36,1,250),(61,2,36,3,300),(62,1,37,2,250),(63,2,37,3,300),(64,1,38,2,250),(65,1,39,2,250),(66,1,40,1,250),(67,1,41,1,250),(68,1,42,1,250),(69,1,43,1,250),(70,100,43,1,120),(71,1,44,1,250),(72,100,44,1,120),(73,1,45,1,250),(74,1,46,1,250),(75,1,47,1,250),(76,1,48,2,250),(77,100,48,1,120),(81,1,50,1,250),(82,1,51,1,250),(83,1,52,1,250),(84,1,53,1,250),(85,1,54,2,250),(86,1,55,2,250),(87,1,56,2,250),(88,1,57,2,250),(89,1,58,2,250),(90,1,59,2,250),(91,1,60,1,250),(92,1,61,1,250),(93,1,62,1,250),(94,1,63,4,122.1),(95,1,64,1,250),(96,1,65,1,250),(97,1,66,1,250),(98,1,67,1,250),(99,1,68,1,250),(100,1,69,1,250),(101,100,70,1,120),(106,100,73,1,120),(111,100,76,2,120),(112,100,77,2,120),(113,100,78,2,120),(114,1,79,1,250),(115,1,80,1,250),(116,1,81,1,250),(117,100,81,1,120),(118,1,82,2,250),(119,100,82,1,120),(120,1,83,1,250),(121,100,83,1,120),(122,1,84,1,250),(123,1,85,5,250),(124,100,85,2,120),(127,1,87,1,250),(128,1,88,1,250),(129,1,89,1,250),(130,1,90,1,250),(131,1,91,1,250),(132,1,92,1,250),(133,1,93,1,250),(134,1,94,1,250),(135,100,95,1,120),(136,1,96,1,250),(137,1,97,1,250),(138,1,98,1,250),(139,1,99,1,250),(140,1,100,1,250),(141,1,101,1,250),(142,1,102,4,122.1),(143,1,103,1,250),(144,1,104,1,250),(145,1,105,1,250),(146,1,106,1,250),(147,100,106,1,120),(148,1,107,3,250),(149,1,108,2,250),(150,1,109,1,250),(151,1,110,1,250),(152,1,111,1,250),(153,1,112,1,250),(154,1,113,1,250),(155,100,113,2,120),(156,1,114,1,250),(157,2,114,1,300),(158,1,115,1,250),(159,1,116,1,250),(160,1,117,1,250),(161,1,118,2,250),(162,100,119,2,120),(163,2,119,2,300),(168,100,122,3,120),(169,1,122,1,250),(170,2,122,2,300),(171,100,123,2,120),(172,1,124,1,250),(173,2,124,2,300),(174,100,124,1,120),(175,100,125,2,120),(176,1,125,1,250),(177,2,125,4,300),(178,1,126,2,250),(179,2,126,1,300),(180,100,126,1,120),(181,100,127,1,120),(182,100,128,2,120),(183,100,129,2,120),(184,1,129,3,250),(185,1,130,1,250),(186,100,131,1,120),(187,1,131,1,250),(188,100,132,1,120),(189,1,132,2,250),(190,1,133,1,250),(191,1,134,1,250),(192,100,134,2,120),(193,1,135,1,250),(194,100,135,2,120),(195,1,136,1,250),(196,100,136,2,120),(197,1,137,1,250),(198,100,137,2,120),(199,1,138,2,250),(200,1,139,2,250),(201,1,140,2,250),(202,1,141,1,250),(203,1,142,1,250),(204,100,142,2,120),(205,1,143,1,250),(206,1,144,1,250),(207,1,145,1,250),(208,1,146,1,250),(209,1,147,2,250),(210,1,148,1,250),(211,1,149,1,250),(212,1,150,1,250),(213,1,151,1,250),(214,1,152,2,250),(215,100,152,1,120),(216,1,153,2,250),(217,100,153,1,120),(218,1,154,1,250),(219,1,155,2,250),(220,100,155,2,120),(221,2,155,1,300),(222,1,156,2,250),(223,100,156,1,120),(224,1,157,2,250),(225,2,157,4,300),(226,100,157,2,120),(227,2,158,1,300),(228,100,158,2,120),(229,1,159,1,250),(230,1,160,1,250),(231,1,161,2,250),(232,100,161,2,120),(233,2,161,1,300),(234,2,162,2,300),(235,100,163,1,120),(236,1,164,2,250),(237,1,165,1,250);
/*!40000 ALTER TABLE `Item_has_Order_Detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Order_Status`
--

DROP TABLE IF EXISTS `Order_Status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Order_Status` (
  `Order_Status_Id` smallint NOT NULL AUTO_INCREMENT,
  `Order_Status_Type` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`Order_Status_Id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Order_Status`
--

LOCK TABLES `Order_Status` WRITE;
/*!40000 ALTER TABLE `Order_Status` DISABLE KEYS */;
INSERT INTO `Order_Status` VALUES (1,'--'),(2,'En proceso'),(3,'Entregado'),(4,'Cancelado'),(5,'En procese de devolución'),(6,'En camino'),(7,'Hubo un error grave'),(8,'Hubo un error en los datos del pedido');
/*!40000 ALTER TABLE `Order_Status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Orders`
--

DROP TABLE IF EXISTS `Orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Orders` (
  `Order_Id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `Order_PayerId` bigint unsigned NOT NULL,
  `Order_HeadquarterId` mediumint unsigned NOT NULL DEFAULT '1',
  `Order_Status_Order_Status_Id` smallint NOT NULL DEFAULT '2',
  `Order_Method_Of_Delivery` varchar(25) NOT NULL,
  `Order_Payment_Method_Opts` varchar(45) NOT NULL,
  `Order_Payment_Total_Price` float unsigned NOT NULL,
  `Order_Date` date DEFAULT NULL,
  `Order_Time` time DEFAULT NULL,
  `Order_CodeMP` int DEFAULT NULL,
  `Order_Branch_Id` mediumint unsigned NOT NULL,
  `Order_Headquarters_Id` mediumint unsigned NOT NULL,
  PRIMARY KEY (`Order_Id`),
  KEY `fk_Order_Payer1_idx` (`Order_PayerId`),
  KEY `fk_Order_Status_idx` (`Order_Status_Order_Status_Id`),
  KEY `fk_Orders_Branch_has_Headquarters1_idx` (`Order_Branch_Id`,`Order_Headquarters_Id`),
  CONSTRAINT `fk_Order_Payer` FOREIGN KEY (`Order_PayerId`) REFERENCES `Payer` (`Payer_Id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `fk_Order_Status` FOREIGN KEY (`Order_Status_Order_Status_Id`) REFERENCES `Order_Status` (`Order_Status_Id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `fk_Orders_Branch_has_Headquarters1` FOREIGN KEY (`Order_Branch_Id`, `Order_Headquarters_Id`) REFERENCES `Branch_has_Headquarters` (`Branch_Branch_Id`, `Headquarters_Headquarters_Id`)
) ENGINE=InnoDB AUTO_INCREMENT=166 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Orders`
--

LOCK TABLES `Orders` WRITE;
/*!40000 ALTER TABLE `Orders` DISABLE KEYS */;
INSERT INTO `Orders` VALUES (36,54855645,2,4,'Delivery','Tarjeta',550,'2021-01-04','16:52:50',NULL,0,0),(37,1155647,1,2,'Retiro en sucursal','Efectivo',1400,'2021-01-04','16:55:25',NULL,0,0),(38,345666777,1,2,'Retiro en sucursal','Efectivo',500,'2021-02-17','16:40:24',NULL,0,0),(39,345666777,1,2,'Retiro en sucursal','Efectivo',500,'2021-02-17','16:48:06',NULL,0,0),(40,345666777,1,2,'Retiro en sucursal','Tarjeta',250,'2021-02-17','18:10:20',NULL,0,0),(41,345666777,1,2,'Retiro en sucursal','Efectivo',250,'2021-02-17','18:19:06',NULL,0,0),(42,345666777,1,2,'Retiro en sucursal','Efectivo',250,'2021-02-17','18:19:58',NULL,0,0),(43,345666777666,1,2,'Delivery','Efectivo',420,'2021-02-17','21:27:22',NULL,0,0),(44,345666777666,1,2,'Delivery','Efectivo',420,'2021-02-17','22:14:00',NULL,0,0),(45,345666777666,1,2,'Retiro en sucursal','Efectivo',250,'2021-02-17','22:16:56',NULL,0,0),(46,345666777666,1,2,'Retiro en sucursal','Efectivo',250,'2021-02-17','22:27:25',NULL,0,0),(47,345666777666,1,2,'Retiro en sucursal','Efectivo',250,'2021-02-17','22:28:06',NULL,0,0),(48,1145567,1,2,'Retiro en sucursal','Tarjeta',620,'2021-02-18','01:36:25',NULL,0,0),(50,55677,1,2,'Retiro en sucursal','Tarjeta',250,'2021-02-18','13:44:05',NULL,0,0),(51,55677,1,2,'Retiro en sucursal','Efectivo',250,'2021-02-18','13:57:11',NULL,0,0),(52,55677,1,2,'Retiro en sucursal','Efectivo',250,'2021-02-18','14:02:51',NULL,0,0),(53,556777,1,2,'Retiro en sucursal','Efectivo',250,'2021-02-18','14:03:10',NULL,0,0),(54,556777,1,2,'Retiro en sucursal','Efectivo',500,'2021-02-18','14:04:32',NULL,0,0),(55,556777,1,2,'Retiro en sucursal','Efectivo',500,'2021-02-18','14:04:53',NULL,0,0),(56,556777,1,2,'Retiro en sucursal','Tarjeta',500,'2021-02-18','14:06:24',NULL,0,0),(57,556777,1,2,'Retiro en sucursal','Tarjeta',500,'2021-02-18','14:08:26',NULL,0,0),(58,556777,1,2,'Retiro en sucursal','Efectivo',500,'2021-02-18','14:08:40',NULL,0,0),(59,556777,1,2,'Retiro en sucursal','Efectivo',500,'2021-02-18','14:08:53',NULL,0,0),(60,556777,1,2,'Retiro en sucursal','Efectivo',250,'2021-02-18','14:18:53',NULL,0,0),(61,556777,1,2,'Retiro en sucursal','Efectivo',250,'2021-02-18','14:21:56',NULL,0,0),(62,556777,1,2,'Retiro en sucursal','Efectivo',250,'2021-02-18','14:22:32',NULL,0,0),(63,2165162643,1,2,'Hello','Hello',122.1,'2021-03-02','04:44:31',NULL,0,0),(64,883933,1,2,'Retiro en sucursal','Efectivo',250,'2021-03-02','04:53:07',NULL,0,0),(65,88393388,1,2,'Retiro en sucursal','Efectivo',250,'2021-03-02','06:11:11',NULL,0,0),(66,88393388,1,2,'Delivery','Efectivo',300,'2021-03-02','06:15:37',NULL,0,0),(67,2288,1,2,'Retiro en sucursal','Efectivo',250,'2021-03-02','06:25:18',NULL,0,0),(68,2288,1,2,'Retiro en sucursal','Efectivo',250,'2021-03-02','06:27:11',NULL,0,0),(69,2288,1,2,'Retiro en sucursal','Efectivo',250,'2021-03-02','06:28:07',NULL,0,0),(70,2288,1,2,'Retiro en sucursal','Efectivo',120,'2021-03-02','06:31:52',NULL,0,0),(73,2288,1,2,'Retiro en sucursal','Efectivo',120,'2021-03-02','06:42:38',NULL,0,0),(76,2288,1,2,'Retiro en sucursal','Efectivo',240,'2021-03-02','06:47:38',NULL,0,0),(77,2288,1,2,'Retiro en sucursal','Efectivo',240,'2021-03-02','06:49:36',NULL,0,0),(78,2288,1,2,'Retiro en sucursal','Efectivo',240,'2021-03-02','06:49:42',NULL,0,0),(79,1183839393,1,2,'Retiro en sucursal','Efectivo',250,'2021-03-02','18:25:38',NULL,0,0),(80,1183839393,1,2,'Retiro en sucursal','Efectivo',250,'2021-03-02','19:45:07',NULL,0,0),(81,828381919,1,2,'Retiro en sucursal','Efectivo',370,'2021-03-02','23:54:43',NULL,0,0),(82,828381919,1,2,'Retiro en sucursal','Efectivo',620,'2021-03-03','15:43:02',NULL,0,0),(83,828381919,1,2,'Retiro en sucursal','Efectivo',370,'2021-03-03','16:12:19',NULL,0,0),(84,828381919,1,2,'Retiro en sucursal','Efectivo',250,'2021-03-03','18:02:30',NULL,0,0),(85,828381919,1,2,'Retiro en sucursal','Efectivo',1490,'2021-03-03','19:21:13',NULL,0,0),(87,118493949,1,2,'Retiro en sucursal','Efectivo',250,'2021-03-03','22:00:40',NULL,0,0),(88,11849494,1,2,'Retiro en sucursal','Efectivo',250,'2021-03-04','04:27:24',NULL,0,0),(89,11849494,1,2,'Retiro en sucursal','Efectivo',250,'2021-03-04','04:30:48',NULL,0,0),(90,11849494,1,2,'Retiro en sucursal','Efectivo',250,'2021-03-04','04:44:45',NULL,0,0),(91,11849494,1,2,'Retiro en sucursal','Efectivo',250,'2021-03-04','04:47:40',NULL,0,0),(92,11849494,1,2,'Retiro en sucursal','Efectivo',250,'2021-03-04','04:48:29',NULL,0,0),(93,11849494,1,2,'Retiro en sucursal','Efectivo',250,'2021-03-04','04:48:56',NULL,0,0),(94,11849494,1,2,'Retiro en sucursal','Efectivo',250,'2021-03-04','04:50:28',NULL,0,0),(95,11849494,1,2,'Retiro en sucursal','Efectivo',120,'2021-03-04','04:52:13',NULL,0,0),(96,11849494,1,2,'Retiro en sucursal','Efectivo',250,'2021-03-04','04:53:57',NULL,0,0),(97,11849494,1,2,'Retiro en sucursal','Efectivo',250,'2021-03-04','05:02:52',NULL,0,0),(98,11849494,1,2,'Retiro en sucursal','Efectivo',250,'2021-03-04','05:06:35',NULL,0,0),(99,11849494,1,2,'Retiro en sucursal','Efectivo',250,'2021-03-04','05:14:55',NULL,0,0),(100,15283838,1,2,'Retiro en sucursal','Efectivo',250,'2021-03-04','07:06:47',NULL,0,0),(101,15283838,1,2,'Retiro en sucursal','Efectivo',250,'2021-03-04','07:08:31',NULL,0,0),(102,2165162643,1,2,'Hello','Hello',122.1,'2021-03-04','17:13:36',NULL,0,0),(103,17772828,1,2,'Retiro en sucursal','Efectivo',250,'2021-03-04','17:30:10',NULL,0,0),(104,17772828,1,2,'Retiro en sucursal','Efectivo',250,'2021-03-04','19:23:40',NULL,0,0),(105,17772828,1,2,'Retiro en sucursal','Efectivo',250,'2021-03-04','19:23:54',NULL,0,0),(106,15282939,1,2,'Retiro en sucursal','Efectivo',370,'2021-03-04','19:45:57',NULL,0,0),(107,1783838,1,4,'Retiro en sucursal','Efectivo',750,'2021-03-04','20:03:00',NULL,0,0),(108,1783838,1,2,'Retiro en sucursal','Efectivo',500,'2021-03-04','20:03:33',NULL,0,0),(109,1783838,1,2,'Retiro en sucursal','Efectivo',250,'2021-03-04','20:27:26',NULL,0,0),(110,1783838,1,2,'Retiro en sucursal','Efectivo',250,'2021-03-04','20:38:46',NULL,0,0),(111,1783838,1,2,'Retiro en sucursal','Efectivo',250,'2021-03-04','20:52:18',NULL,0,0),(112,1783838,1,2,'Retiro en sucursal','Efectivo',250,'2021-03-04','21:35:11',NULL,0,0),(113,159292929,1,2,'Retiro en sucursal','Efectivo',490,'2021-03-05','02:20:48',NULL,0,0),(114,11392929,1,2,'Retiro en sucursal','Efectivo',550,'2021-03-05','02:25:38',NULL,0,0),(115,178,1,2,'Retiro en sucursal','Efectivo',250,'2021-03-06','00:07:45',NULL,0,0),(116,56888,1,2,'Retiro en sucursal','Efectivo',250,'2021-03-08','02:51:41',NULL,0,0),(117,568988,1,2,'Retiro en sucursal','Efectivo',250,'2021-03-08','16:20:33',NULL,0,0),(118,568988,1,2,'Retiro en sucursal','Efectivo',500,'2021-03-08','16:31:48',NULL,0,0),(119,1538383930,1,2,'Retiro en sucursal','Efectivo',840,'2021-03-09','01:55:11',NULL,0,0),(122,1538383930,1,2,'Retiro en sucursal','Efectivo',1210,'2021-03-09','04:37:48',NULL,0,0),(123,116728282,1,4,'Delivery','Efectivo',290,'2021-03-09','06:11:17',NULL,0,0),(124,116728282,1,2,'Retiro en sucursal','Efectivo',970,'2021-03-09','06:24:08',NULL,0,0),(125,116728282,1,2,'Retiro en sucursal','Efectivo',1690,'2021-03-09','16:38:48',NULL,0,0),(126,153938283,1,2,'Retiro en sucursal','Efectivo',920,'2021-03-09','20:20:33',NULL,0,0),(127,153938283,1,2,'Retiro en sucursal','Efectivo',120,'2021-03-09','21:04:38',NULL,0,0),(128,153938283,1,2,'Retiro en sucursal','Efectivo',240,'2021-03-09','21:31:03',NULL,0,0),(129,15598283,1,2,'Retiro en sucursal','Efectivo',990,'2021-03-10','01:49:58',NULL,0,0),(130,153857378,1,2,'Retiro en sucursal','Efectivo',250,'2021-03-10','02:11:08',NULL,0,0),(131,153857378,1,2,'Retiro en sucursal','Efectivo',370,'2021-03-10','03:01:44',NULL,0,0),(132,153857378,1,2,'Retiro en sucursal','Efectivo',620,'2021-03-10','03:04:43',NULL,0,0),(133,15389383,1,2,'Retiro en sucursal','Efectivo',250,'2021-03-10','03:08:04',NULL,0,0),(134,15389339,1,2,'Retiro en sucursal','Efectivo',490,'2021-03-10','03:08:47',NULL,0,0),(135,15389339,1,2,'Retiro en sucursal','Efectivo',490,'2021-03-10','03:12:27',NULL,0,0),(136,15389339,1,2,'Retiro en sucursal','Efectivo',490,'2021-03-10','03:15:30',NULL,0,0),(137,15389339,1,2,'Retiro en sucursal','Efectivo',490,'2021-03-10','03:15:38',NULL,0,0),(138,171799,1,2,'Retiro en sucursal','Efectivo',500,'2021-03-10','03:24:52',NULL,0,0),(139,171799,1,2,'Retiro en sucursal','Efectivo',500,'2021-03-10','03:25:35',NULL,0,0),(140,1717997,1,2,'Retiro en sucursal','Efectivo',500,'2021-03-10','03:26:25',NULL,0,0),(141,1717997,1,2,'Retiro en sucursal','Efectivo',250,'2021-03-10','03:31:25',NULL,0,0),(142,1717997,1,2,'Retiro en sucursal','Efectivo',490,'2021-03-10','03:31:38',NULL,0,0),(143,1717997,1,2,'Retiro en sucursal','Efectivo',250,'2021-03-10','03:32:35',NULL,0,0),(144,1283838,1,2,'Retiro en sucursal','Efectivo',250,'2021-03-10','03:34:08',NULL,0,0),(145,1283838,1,2,'Retiro en sucursal','Efectivo',250,'2021-03-10','03:34:31',NULL,0,0),(146,1283838,1,2,'Retiro en sucursal','Efectivo',250,'2021-03-10','03:49:01',NULL,0,0),(147,163833949,1,2,'Retiro en sucursal','Efectivo',500,'2021-03-10','04:00:31',NULL,0,0),(148,173834949,1,2,'Retiro en sucursal','Efectivo',250,'2021-03-10','04:20:17',NULL,0,0),(149,173834949,1,2,'Retiro en sucursal','Efectivo',250,'2021-03-10','04:22:04',NULL,0,0),(150,173834949,1,2,'Retiro en sucursal','Efectivo',250,'2021-03-10','04:49:59',NULL,0,0),(151,173834949,1,2,'Retiro en sucursal','Efectivo',250,'2021-03-10','04:50:04',NULL,0,0),(152,15384899,1,2,'Retiro en sucursal','Efectivo',620,'2021-03-10','04:53:13',NULL,0,0),(153,15384899,1,2,'Retiro en sucursal','Efectivo',620,'2021-03-10','04:53:25',NULL,0,0),(154,15384899,1,2,'Retiro en sucursal','Efectivo',250,'2021-03-10','05:14:53',NULL,0,0),(155,15384899,1,2,'Retiro en sucursal','Efectivo',1040,'2021-03-10','05:15:24',NULL,0,0),(156,15394390,1,2,'Retiro en sucursal','Efectivo',620,'2021-03-11','03:28:02',NULL,0,0),(157,1538599282,1,4,'Delivery','Efectivo',1990,'2021-03-11','04:13:38',NULL,0,0),(158,1538599282,1,2,'Retiro en sucursal','Efectivo',540,'2021-03-11','04:14:04',NULL,0,0),(159,15383838,1,2,'Retiro en sucursal','Efectivo',250,'2021-03-11','19:41:32',NULL,0,0),(160,154588568,1,2,'Retiro en sucursal','Efectivo',250,'2021-03-11','19:45:43',NULL,0,0),(161,154588568,1,2,'Delivery','Efectivo',1090,'2021-03-11','19:47:09',NULL,0,0),(162,154588568,1,2,'Delivery','Efectivo',650,'2021-03-11','19:48:20',NULL,0,0),(163,154588568,1,2,'Delivery','Efectivo',170,'2021-03-11','19:55:47',NULL,0,0),(164,154848499,1,2,'Retiro en sucursal','Efectivo',500,'2021-03-11','20:18:00',NULL,0,0),(165,154848499,1,2,'Delivery','Efectivo',300,'2021-03-11','23:43:54',NULL,0,0);
/*!40000 ALTER TABLE `Orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Payer`
--

DROP TABLE IF EXISTS `Payer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Payer` (
  `Payer_Id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `Pay_Email` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `Pay_Name` varchar(30) DEFAULT NULL,
  `Pay_Surname` varchar(30) DEFAULT NULL,
  `Pay_Address_Street_Name` varchar(45) DEFAULT NULL,
  `Pay_Address_Street_Number` mediumint unsigned DEFAULT NULL,
  `Pay_Phone_Number` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`Payer_Id`)
) ENGINE=InnoDB AUTO_INCREMENT=11435644964426249 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Payer`
--

LOCK TABLES `Payer` WRITE;
/*!40000 ALTER TABLE `Payer` DISABLE KEYS */;
INSERT INTO `Payer` VALUES (178,'sja','Lau','Ra',NULL,NULL,NULL),(2288,'j4k3kg@gma.com','Pedr','Shelb',NULL,NULL,NULL),(55677,'bhhh@jujh.com','Ton','Juar',NULL,NULL,NULL),(56888,'gghh','Fgg','Jjhhg',NULL,NULL,NULL),(171799,'jsjssjs@jdkdkd.com','Walas','Masacre',NULL,NULL,NULL),(556777,'bhhh@jujh.com','Ton','Juar',NULL,NULL,NULL),(568988,'ggh1@ahaha.com','Fgge','Jjhhjg',NULL,NULL,NULL),(883933,'liamg@gma.com','Lima','Galagher',NULL,NULL,NULL),(1145567,'dwttyyt@dgtg.com','Fer','B',NULL,NULL,NULL),(1155647,'sad@gmail.com','Juan','Doe',NULL,NULL,NULL),(1283838,'ajsjs@didk.com','Wqlas','Masactr',NULL,NULL,NULL),(1717997,'jsjssjs@jdkdkd','Walas','Masacre',NULL,NULL,NULL),(1783838,'eamome8383@fkfkfk','Ra','Mon',NULL,NULL,NULL),(11392929,'al2n384@gmail.com','A','Lan',NULL,NULL,NULL),(11849494,'merdajk@jjjkkk.com','Mer','Da',NULL,NULL,NULL),(15282939,'laur@hss8.com','Lau','Ra',NULL,NULL,NULL),(15283838,'pedroabr@gmal.com','Pedro','Qbram',NULL,NULL,NULL),(15383838,'fernandoba99@gmail.com','Fernando','Barraza',NULL,NULL,NULL),(15384899,'salas8dif@jdjd','Wa','Las',NULL,NULL,NULL),(15389339,'wala@gmail.com','Walasa','Masacre',NULL,NULL,NULL),(15389383,'fabfk@gmail.com','Gab','RD',NULL,NULL,NULL),(15394390,'fernandid88a@gmail.com','Fernanfo','Nbarrazao',NULL,NULL,NULL),(15598283,'walas83jdkd@sksls','Lwalas ','Masacres',NULL,NULL,NULL),(17772828,'ramon992@gmail.com','Ra','Mon',NULL,NULL,NULL),(54855645,'queonda@gmail.com','Fer','Bar',NULL,NULL,NULL),(88393388,'liamg@gma.com','Lima','Galagher',NULL,NULL,NULL),(116728282,'fenkslla@did.com','Fer','Nando',NULL,NULL,NULL),(118493949,'jpak8iin@gmail.com','Juan','Perz',NULL,NULL,NULL),(153857378,'gabitodj@jddi.com','Gaby','RD',NULL,NULL,NULL),(153938283,'lucas83jdkd@sksls.com','Lu','Cas',NULL,NULL,NULL),(154588568,'ferjsjsk@gmaiil.com','Fernando','Barr',NULL,NULL,NULL),(154848499,'fernandobarraza99@gmail.com','Fernando','Barraza',NULL,NULL,NULL),(159292929,'ramon29384@gmail.com','Ra','Mon',NULL,NULL,NULL),(163833949,'allandj88d@slsls.com','A','Lan',NULL,NULL,NULL),(173834949,'teresakd@msks.com','Tere','Sa',NULL,NULL,NULL),(345666777,'ton@hjjh.com','Tony','Stark',NULL,NULL,NULL),(828381919,'kdkdkd@gmail.com','Pedro','Bar',NULL,NULL,NULL),(1183839393,'mled@gmailm.com','Mri','Led',NULL,NULL,NULL),(1538383930,'sjjsksk@dkde.com','A','Lan',NULL,NULL,NULL),(1538599282,'fernandobar9@gmail.com','Fernando','Barraza',NULL,NULL,NULL),(2165162643,'Hello','Hello','Hello',NULL,NULL,NULL),(345666777666,'ton@hjjh.com','Tony','Stark',NULL,NULL,NULL);
/*!40000 ALTER TABLE `Payer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Url_images`
--

DROP TABLE IF EXISTS `Url_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Url_images` (
  `Url_images` bigint unsigned NOT NULL AUTO_INCREMENT,
  `Url_images_Location` varchar(25) NOT NULL,
  `Item_Item_Id` bigint unsigned NOT NULL,
  PRIMARY KEY (`Url_images`),
  KEY `fk_Url_images_Item1_idx` (`Item_Item_Id`),
  CONSTRAINT `fk_Url_images_Item1` FOREIGN KEY (`Item_Item_Id`) REFERENCES `Item` (`Item_Id`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Url_images`
--

LOCK TABLES `Url_images` WRITE;
/*!40000 ALTER TABLE `Url_images` DISABLE KEYS */;
/*!40000 ALTER TABLE `Url_images` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-03-14 13:01:05
