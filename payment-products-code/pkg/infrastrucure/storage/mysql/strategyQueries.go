package mysql

import (
	"context"
	//"database/sql"
	"fabrikapps/payment-products-code/pkg/domain/product"
	"log"
	"strconv"
)

type PaymentStrategy interface {
	InsertPayment(ctx context.Context, repoR *mysqlProductsRepository) (bool, error, uint64)
}
type InsertPaymentWithDeliveryAndEfectivo struct {
	reqPaymentToInsert *product.PaymentToProcess
}
type InsertPaymentWithDeliveryAndTarjeta struct {
	reqPaymentToInsert *product.PaymentToProcess
}
type InsertPaymentWithoutDeliveryAndEfectivo struct {
	reqPaymentToInsert *product.PaymentToProcess
}
type InsertPaymentWithoutDeliveryAndTarjeta struct {
	reqPaymentToInsert *product.PaymentToProcess
}

func (i *InsertPaymentWithDeliveryAndEfectivo) InsertPayment(ctx context.Context, repoR *mysqlProductsRepository) (bool, error, uint64) {
	phoneNumber, err := strconv.Atoi(i.reqPaymentToInsert.Payer.Phone.Number)
	tx, err := repoR.db.BeginTx(ctx, nil)
	if err != nil {
		// Incase we find any error in the query execution, rollback the transaction
		tx.Rollback()
		return false, err, 0
	}

	_, err = tx.ExecContext(ctx, "INSERT INTO payment_checkout_db4.Payer (Payer_Id, Pay_Email, Pay_Name, Pay_Surname, Pay_Phone_Number) VALUES(?, ?, ?, ?,?) ON DUPLICATE KEY UPDATE Pay_Email=?, Pay_Name=?, Pay_Surname=?;", phoneNumber, i.reqPaymentToInsert.Payer.Email, i.reqPaymentToInsert.Payer.Name, i.reqPaymentToInsert.Payer.Surname, i.reqPaymentToInsert.Payer.Phone.Number, i.reqPaymentToInsert.Payer.Email, i.reqPaymentToInsert.Payer.Name, i.reqPaymentToInsert.Payer.Surname)
	if err != nil {
		tx.Rollback()
		return false, err, 0
	}

	_, err = tx.ExecContext(ctx, "INSERT INTO payment_checkout_db4.Delivery_Address (Delivery_Address_Street_Name, Delivery_Address_Street_Number) VALUES(?, ?);", i.reqPaymentToInsert.Payer.Address.StreetName, i.reqPaymentToInsert.Payer.Address.StreetNumber)
	if err != nil {
		tx.Rollback()
		return false, err, 0
	}
	_, err = tx.ExecContext(ctx, "SET @last_id_in_Del_Ad = LAST_INSERT_ID();")
	if err != nil {
		tx.Rollback()
		return false, err, 0
	}

	res, err := tx.ExecContext(ctx, "INSERT INTO payment_checkout_db4.`Orders` (Order_PayerId, Order_Branch_Id, Order_Headquarters_Id, Order_Status_Order_Status_Id, Order_Method_Of_Delivery, Order_Payment_Method_Opts, Order_Payment_Total_Price, Order_Date, Order_Time, Order_CodeMP, Order_Address_Id) VALUES(?, ?, ?, 2, ?, ?, ?, CURDATE(), CURTIME(), ?, @last_id_in_Del_Ad);", phoneNumber, i.reqPaymentToInsert.BranchId, i.reqPaymentToInsert.HeadquartersId, i.reqPaymentToInsert.MethodOfDelivery, i.reqPaymentToInsert.PaymentMethodOptions, i.reqPaymentToInsert.TotalPrice, 0)
	if err != nil {
		tx.Rollback()
		return false, err, 0
	}
	_, err = tx.ExecContext(ctx, "SET @last_id_in_Orders = LAST_INSERT_ID();")
	if err != nil {
		tx.Rollback()
		return false, err, 0
	}
	for _, item := range i.reqPaymentToInsert.ListaItems {
		_, err = tx.ExecContext(ctx, "INSERT INTO payment_checkout_db4.Item_has_Order_Detail (Item_Item_Id, Order_Order_Id, Item_has_Order_Detail_Quantity, Item_has_Order_Detail_Unit_Price) VALUES(?, @last_id_in_Orders, ?, ?);", item.ItemId, item.Quantity, item.UnitPrice)
		if err != nil {
			tx.Rollback()
			return false, err, 0
		}
	}
	err = tx.Commit()
	if err != nil {
		log.Fatal(err)
		return false, err, 0
	}
	var orderCode, _ = res.LastInsertId()

	return true, nil, uint64(orderCode)
}
func (i *InsertPaymentWithDeliveryAndTarjeta) InsertPayment(ctx context.Context, repoR *mysqlProductsRepository) (bool, error, uint64) {
	phoneNumber, err := strconv.Atoi(i.reqPaymentToInsert.Payer.Phone.Number)
	tx, err := repoR.db.BeginTx(ctx, nil)
	if err != nil {
		// Incase we find any error in the query execution, rollback the transaction
		tx.Rollback()
		return false, err, 0
	}

	_, err = tx.ExecContext(ctx, "INSERT INTO payment_checkout_db4.Payer (Payer_Id, Pay_Email, Pay_Name, Pay_Surname, Pay_Phone_Number) VALUES(?, ?, ?, ?,?) ON DUPLICATE KEY UPDATE Pay_Email=?, Pay_Name=?, Pay_Surname=?;", phoneNumber, i.reqPaymentToInsert.Payer.Email, i.reqPaymentToInsert.Payer.Name, i.reqPaymentToInsert.Payer.Surname, i.reqPaymentToInsert.Payer.Phone.Number, i.reqPaymentToInsert.Payer.Email, i.reqPaymentToInsert.Payer.Name, i.reqPaymentToInsert.Payer.Surname)
	if err != nil {
		tx.Rollback()
		return false, err, 0
	}

	_, err = tx.ExecContext(ctx, "INSERT INTO payment_checkout_db4.Delivery_Address (Delivery_Address_Street_Name, Delivery_Address_Street_Number) VALUES(?, ?);", i.reqPaymentToInsert.Payer.Address.StreetName, i.reqPaymentToInsert.Payer.Address.StreetNumber)
	if err != nil {
		tx.Rollback()
		return false, err, 0
	}
	_, err = tx.ExecContext(ctx, "SET @last_id_in_Del_Ad = LAST_INSERT_ID();")
	if err != nil {
		tx.Rollback()
		return false, err, 0
	}

	res, err := tx.ExecContext(ctx, "INSERT INTO payment_checkout_db4.`Orders` (Order_PayerId, Order_Branch_Id, Order_Headquarters_Id, Order_Status_Order_Status_Id, Order_Method_Of_Delivery, Order_Payment_Method_Opts, Order_Payment_Total_Price, Order_Date, Order_Time, Order_CodeMP, Order_Address_Id) VALUES(?, ?, ?, 2, ?, ?, ?, CURDATE(), CURTIME(), ?, @last_id_in_Del_Ad);", phoneNumber, i.reqPaymentToInsert.BranchId, i.reqPaymentToInsert.HeadquartersId, i.reqPaymentToInsert.MethodOfDelivery, i.reqPaymentToInsert.PaymentMethodOptions, i.reqPaymentToInsert.TotalPrice, i.reqPaymentToInsert.OrderMpGenerated)
	if err != nil {
		tx.Rollback()
		return false, err, 0
	}
	_, err = tx.ExecContext(ctx, "SET @last_id_in_Orders = LAST_INSERT_ID();")
	if err != nil {
		tx.Rollback()
		return false, err, 0
	}
	for _, item := range i.reqPaymentToInsert.ListaItems {
		_, err = tx.ExecContext(ctx, "INSERT INTO payment_checkout_db4.Item_has_Order_Detail (Item_Item_Id, Order_Order_Id, Item_has_Order_Detail_Quantity, Item_has_Order_Detail_Unit_Price) VALUES(?, @last_id_in_Orders, ?, ?);", item.ItemId, item.Quantity, item.UnitPrice)
		if err != nil {
			tx.Rollback()
			return false, err, 0
		}
	}
	err = tx.Commit()
	if err != nil {
		log.Fatal(err)
		return false, err, 0
	}
	var orderCode, _ = res.LastInsertId()

	return true, nil, uint64(orderCode)
}
func (i *InsertPaymentWithoutDeliveryAndEfectivo) InsertPayment(ctx context.Context, repoR *mysqlProductsRepository) (bool, error, uint64) {
	phoneNumber, err := strconv.Atoi(i.reqPaymentToInsert.Payer.Phone.Number)
	tx, err := repoR.db.BeginTx(ctx, nil)
	if err != nil {
		// Incase we find any error in the query execution, rollback the transaction
		tx.Rollback()
		return false, err, 0
	}

	_, err = tx.ExecContext(ctx, "INSERT INTO payment_checkout_db4.Payer (Payer_Id, Pay_Email, Pay_Name, Pay_Surname, Pay_Phone_Number) VALUES(?, ?, ?, ?,?) ON DUPLICATE KEY UPDATE Pay_Email=?, Pay_Name=?, Pay_Surname=?;", phoneNumber, i.reqPaymentToInsert.Payer.Email, i.reqPaymentToInsert.Payer.Name, i.reqPaymentToInsert.Payer.Surname, i.reqPaymentToInsert.Payer.Phone.Number, i.reqPaymentToInsert.Payer.Email, i.reqPaymentToInsert.Payer.Name, i.reqPaymentToInsert.Payer.Surname)
	if err != nil {
		tx.Rollback()
		return false, err, 0
	}

	_, err = tx.ExecContext(ctx, "SET @last_id_in_Del_Ad = 0;")
	if err != nil {
		tx.Rollback()
		return false, err, 0
	}

	res, err := tx.ExecContext(ctx, "INSERT INTO payment_checkout_db4.`Orders` (Order_PayerId, Order_Branch_Id, Order_Headquarters_Id, Order_Status_Order_Status_Id, Order_Method_Of_Delivery, Order_Payment_Method_Opts, Order_Payment_Total_Price, Order_Date, Order_Time, Order_CodeMP, Order_Address_Id) VALUES(?, ?, ?, 2, ?, ?, ?, CURDATE(), CURTIME(), ?, @last_id_in_Del_Ad);", phoneNumber, i.reqPaymentToInsert.BranchId, i.reqPaymentToInsert.HeadquartersId, i.reqPaymentToInsert.MethodOfDelivery, i.reqPaymentToInsert.PaymentMethodOptions, i.reqPaymentToInsert.TotalPrice, 0)
	if err != nil {
		tx.Rollback()
		return false, err, 0
	}
	_, err = tx.ExecContext(ctx, "SET @last_id_in_Orders = LAST_INSERT_ID();")
	if err != nil {
		tx.Rollback()
		return false, err, 0
	}
	for _, item := range i.reqPaymentToInsert.ListaItems {
		_, err = tx.ExecContext(ctx, "INSERT INTO payment_checkout_db4.Item_has_Order_Detail (Item_Item_Id, Order_Order_Id, Item_has_Order_Detail_Quantity, Item_has_Order_Detail_Unit_Price) VALUES(?, @last_id_in_Orders, ?, ?);", item.ItemId, item.Quantity, item.UnitPrice)
		if err != nil {
			tx.Rollback()
			return false, err, 0
		}
	}
	err = tx.Commit()
	if err != nil {
		log.Fatal(err)
		return false, err, 0
	}
	var orderCode, _ = res.LastInsertId()

	return true, nil, uint64(orderCode)
}
func (i *InsertPaymentWithoutDeliveryAndTarjeta) InsertPayment(ctx context.Context, repoR *mysqlProductsRepository) (bool, error, uint64) {
	phoneNumber, err := strconv.Atoi(i.reqPaymentToInsert.Payer.Phone.Number)
	tx, err := repoR.db.BeginTx(ctx, nil)
	if err != nil {
		// Incase we find any error in the query execution, rollback the transaction
		tx.Rollback()
		return false, err, 0
	}

	_, err = tx.ExecContext(ctx, "INSERT INTO payment_checkout_db4.Payer (Payer_Id, Pay_Email, Pay_Name, Pay_Surname, Pay_Phone_Number) VALUES(?, ?, ?, ?,?) ON DUPLICATE KEY UPDATE Pay_Email=?, Pay_Name=?, Pay_Surname=?;", phoneNumber, i.reqPaymentToInsert.Payer.Email, i.reqPaymentToInsert.Payer.Name, i.reqPaymentToInsert.Payer.Surname, i.reqPaymentToInsert.Payer.Phone.Number, i.reqPaymentToInsert.Payer.Email, i.reqPaymentToInsert.Payer.Name, i.reqPaymentToInsert.Payer.Surname)
	if err != nil {
		tx.Rollback()
		return false, err, 0
	}

	_, err = tx.ExecContext(ctx, "SET @last_id_in_Del_Ad = 0;")
	if err != nil {
		tx.Rollback()
		return false, err, 0
	}

	res, err := tx.ExecContext(ctx, "INSERT INTO payment_checkout_db4.`Orders` (Order_PayerId, Order_Branch_Id, Order_Headquarters_Id, Order_Status_Order_Status_Id, Order_Method_Of_Delivery, Order_Payment_Method_Opts, Order_Payment_Total_Price, Order_Date, Order_Time, Order_CodeMP, Order_Address_Id) VALUES(?, ?, ?, 2, ?, ?, ?, CURDATE(), CURTIME(), ?, @last_id_in_Del_Ad);", phoneNumber, i.reqPaymentToInsert.BranchId, i.reqPaymentToInsert.HeadquartersId, i.reqPaymentToInsert.MethodOfDelivery, i.reqPaymentToInsert.PaymentMethodOptions, i.reqPaymentToInsert.TotalPrice, i.reqPaymentToInsert.OrderMpGenerated)
	if err != nil {
		tx.Rollback()
		return false, err, 0
	}
	_, err = tx.ExecContext(ctx, "SET @last_id_in_Orders = LAST_INSERT_ID();")
	if err != nil {
		tx.Rollback()
		return false, err, 0
	}
	for _, item := range i.reqPaymentToInsert.ListaItems {
		_, err = tx.ExecContext(ctx, "INSERT INTO payment_checkout_db4.Item_has_Order_Detail (Item_Item_Id, Order_Order_Id, Item_has_Order_Detail_Quantity, Item_has_Order_Detail_Unit_Price) VALUES(?, @last_id_in_Orders, ?, ?);", item.ItemId, item.Quantity, item.UnitPrice)
		if err != nil {
			tx.Rollback()
			return false, err, 0
		}
	}
	err = tx.Commit()
	if err != nil {
		log.Fatal(err)
		return false, err, 0
	}
	var orderCode, _ = res.LastInsertId()

	return true, nil, uint64(orderCode)
}

type PaymentChangeStatusStrategy interface {
	ChangeOrderPaymentRepo(ctx context.Context, repoR *mysqlProductsRepository) (bool, error)
}
type ChangeOrderStatus struct {
	reqNewStateOrder        uint64
	reqOrderPaymentToChange uint64
}

func (c *ChangeOrderStatus) ChangeOrderPaymentRepo(ctx context.Context, repoR *mysqlProductsRepository) (bool, error) {
	r, err := repoR.db.Exec("UPDATE payment_checkout_db4.`Orders` SET Order_Status_Order_Status_Id=? WHERE Order_Id=?;", c.reqNewStateOrder, c.reqOrderPaymentToChange)
	if err != nil {
		log.Printf("Error %s when creating DB\n", err)
		return false, err
	}
	rowsAffected, _ := r.RowsAffected()
	if rowsAffected == 0 {
		log.Printf("No Updates or no exist DB\n")
		return false, err
	}
	return true, nil
}
