package mysql

import (
	"context"
	"database/sql"
	"fabrikapps/payment-products-code/pkg/domain/product"
	"log"
	//"strconv"
)

const (
	mysqlSelectOrdersQuery = `SELECT Payer_Id, Pay_Email, Pay_Name, Pay_Surname, 
	Delivery_Address_Street_Name, Delivery_Address_Street_Number,
	Pay_Phone_Number,
 	Order_Method_Of_Delivery, Order_Payment_Method_Opts, Order_Payment_Total_Price, Order_Date, Order_Time, Order_Status_Order_Status_Id, Order_Id,
 	Headquarters_Name, 
 	Order_Status_Type
	FROM payment_checkout_db4.Payer Payer
	JOIN payment_checkout_db4.Orders o  ON o.Order_PayerId = Payer.Payer_Id AND Order_Branch_Id = ? AND Order_Headquarters_Id = ?
	JOIN payment_checkout_db4.Delivery_Address da ON da.Delivery_Address_Id = o.Order_Address_Id
	JOIN payment_checkout_db4.Order_Status OS  ON OS.Order_Status_Id = o.Order_Status_Order_Status_Id
	JOIN payment_checkout_db4.Item_has_Order_Detail IhOD  ON IhOD.Order_Order_Id = o.Order_Id
	JOIN payment_checkout_db4.Item I  ON IhOD.Item_Item_Id = I.Item_Id 
		AND I.Branch_has_Headquarters_Branch_Branch_Id = ? 
		AND I.Branch_has_Headquarters_Headquarters_Headquarters_Id = ?
 	JOIN payment_checkout_db4.Branch_has_Headquarters bhh 
 	ON I.Branch_has_Headquarters_Branch_Branch_Id = bhh.Branch_Branch_Id 
 		AND I.Branch_has_Headquarters_Headquarters_Headquarters_Id = bhh.Headquarters_Headquarters_Id 
 		AND bhh.Branch_Branch_Id = ? AND bhh.Headquarters_Headquarters_Id = ?
 	JOIN payment_checkout_db4.Branch B ON B.Branch_Id = bhh.Branch_Branch_Id AND B.Branch_Id = ?
 	JOIN payment_checkout_db4.Headquarters H  ON H.Headquarters_Id = bhh.Headquarters_Headquarters_Id AND H.Headquarters_Id = ?
 	WHERE Order_Date >= DATE_ADD(CURDATE(), INTERVAL -3 DAY);`
	mysqlSelectOrderDetailQuery = `SELECT Item_has_Order_Detail_Quantity, Item_has_Order_Detail_Unit_Price, 
	Item_Id, Item_Title, Item_CategoriasMenu_id, Item_ImageName
	FROM payment_checkout_db4.Payer Payer
		JOIN payment_checkout_db4.Orders o  ON o.Order_PayerId = Payer.Payer_Id 
		JOIN payment_checkout_db4.Item_has_Order_Detail ItemInOrder ON ItemInOrder.Order_Order_Id = o.Order_Id 
		JOIN payment_checkout_db4.Item i ON i.Item_Id = ItemInOrder.Item_Item_Id 
	WHERE Payer_Id = ? AND Order_Id = ?;`

	mysqlSelectSingleOrderQuery = `SELECT Payer_Id, Pay_Email, Pay_Name, Pay_Surname, 
	Delivery_Address_Street_Name, Delivery_Address_Street_Number,
	Pay_Phone_Number,
  	Order_Method_Of_Delivery, Order_Payment_Method_Opts, Order_Payment_Total_Price, Order_Date, Order_Time, Order_Status_Order_Status_Id, Order_Id,
  	Headquarters_Name, 
  	Order_Status_Type
	FROM payment_checkout_db4.Payer Payer
	JOIN payment_checkout_db4.Orders o  ON o.Order_PayerId = Payer.Payer_Id AND Order_Branch_Id = ? AND Order_Headquarters_Id = ?
	JOIN payment_checkout_db4.Delivery_Address da ON da.Delivery_Address_Id = o.Order_Address_Id
	JOIN payment_checkout_db4.Order_Status OS  ON OS.Order_Status_Id = o.Order_Status_Order_Status_Id
	JOIN payment_checkout_db4.Item_has_Order_Detail IhOD  ON IhOD.Order_Order_Id = o.Order_Id
	JOIN payment_checkout_db4.Item I  ON IhOD.Item_Item_Id = I.Item_Id 
		AND I.Branch_has_Headquarters_Branch_Branch_Id = ? 
		AND I.Branch_has_Headquarters_Headquarters_Headquarters_Id = ?
 	JOIN payment_checkout_db4.Branch_has_Headquarters bhh 
 	ON I.Branch_has_Headquarters_Branch_Branch_Id = bhh.Branch_Branch_Id 
 		AND I.Branch_has_Headquarters_Headquarters_Headquarters_Id = bhh.Headquarters_Headquarters_Id 
 		AND bhh.Branch_Branch_Id = ? AND bhh.Headquarters_Headquarters_Id = ?
 	JOIN payment_checkout_db4.Branch B ON B.Branch_Id = bhh.Branch_Branch_Id AND B.Branch_Id = ?
 	JOIN payment_checkout_db4.Headquarters H  ON H.Headquarters_Id = bhh.Headquarters_Headquarters_Id AND H.Headquarters_Id = ?
 	WHERE Order_Branch_Id = ? AND Order_Id = ?;`
	mysqlSelectSingleDetailOrderQuery = `SELECT Item_has_Order_Detail_Quantity, Item_has_Order_Detail_Unit_Price, 
	Item_Id, Item_Title, Item_CategoriasMenu_id, Item_ImageName
	FROM payment_checkout_db4.Payer Payer
		JOIN payment_checkout_db4.Orders o  ON o.Order_PayerId = Payer.Payer_Id 
		JOIN payment_checkout_db4.Item_has_Order_Detail ItemInOrder ON ItemInOrder.Order_Order_Id = o.Order_Id 
		JOIN payment_checkout_db4.Item i ON i.Item_Id = ItemInOrder.Item_Item_Id 
	WHERE Payer_Id = ? AND Order_Id = ?;`
)

// Storage keeps data in db
type mysqlProductsRepository struct {
	db *sql.DB
}

func NewMysqlProductsRepository(db *sql.DB) product.Repository {
	return &mysqlProductsRepository{db}
}

func (repoR *mysqlProductsRepository) InsertPaymentRepo(reqPaymentToInsert *product.PaymentToProcess) (bool, error, uint64) {
	ctx := context.Background()
	var activeStrategy PaymentStrategy

	if reqPaymentToInsert.MethodOfDelivery == "Delivery" && reqPaymentToInsert.PaymentMethodOptions == "Efectivo" {
		activeStrategy = &InsertPaymentWithDeliveryAndEfectivo{reqPaymentToInsert}
	} else if reqPaymentToInsert.MethodOfDelivery == "Delivery" && reqPaymentToInsert.PaymentMethodOptions == "Tarjeta" {
		activeStrategy = &InsertPaymentWithDeliveryAndTarjeta{reqPaymentToInsert}
	} else if reqPaymentToInsert.MethodOfDelivery == "Retiro en sucursal" && reqPaymentToInsert.PaymentMethodOptions == "Efectivo" {
		activeStrategy = &InsertPaymentWithoutDeliveryAndEfectivo{reqPaymentToInsert}
	} else if reqPaymentToInsert.MethodOfDelivery == "Retiro en sucursal" && reqPaymentToInsert.PaymentMethodOptions == "Tarjeta" {
		activeStrategy = &InsertPaymentWithoutDeliveryAndTarjeta{reqPaymentToInsert}
	} else {
		return false, nil, 0
	}

	_, err, orderCode := activeStrategy.InsertPayment(ctx, repoR)

	if err != nil {
		log.Fatal(err)
		return false, err, 0
	}

	return true, nil, orderCode
}
func (repoR *mysqlProductsRepository) ChangeOrderPaymentRepo(reqNewStateOrder uint64, reqOrderPaymentToChange uint64) (bool, error) {
	ctx := context.Background()
	// var changeStatus PaymentChangeStatusStrategy
	var changeStatus PaymentChangeStatusStrategy = &ChangeOrderStatus{reqNewStateOrder, reqOrderPaymentToChange}
	valueState, err := changeStatus.ChangeOrderPaymentRepo(ctx, repoR)
	return valueState, err
}
func (repoR *mysqlProductsRepository) CancelOrderPaymentRepo(reqOrderPaymentToChange uint64) (bool, error) {
	ctx := context.Background()
	// var changeStatus PaymentChangeStatusStrategy
	var changeStatus PaymentChangeStatusStrategy = &ChangeOrderStatus{4, reqOrderPaymentToChange}
	valueState, err := changeStatus.ChangeOrderPaymentRepo(ctx, repoR)
	return valueState, err
}
func (repoR *mysqlProductsRepository) SelectLastOrdersRepo(branchId uint32, headquarterId uint32) ([]*product.OrdersRequest, error) {
	res, err := repoR.db.Query(mysqlSelectOrdersQuery, branchId, headquarterId, branchId, headquarterId, branchId, headquarterId, branchId, headquarterId)
	if err != nil {
		log.Printf("Error %s when creating DB\n", err)
		return nil, err
	}
	defer res.Close()
	var lastOrders []*product.OrdersRequest
	for res.Next() {
		var lastOrderDetail []*product.ItemDetailResponse
		var currOrder = &product.OrdersRequest{}
		//var algo int
		err := res.Scan(&currOrder.PayerId, &currOrder.Email, &currOrder.Name, &currOrder.Surname,
			&currOrder.StreetName, &currOrder.StreetNumber, &currOrder.PhoneNumber, &currOrder.MethodOfDelivery,
			&currOrder.PaymentMethodOptions, &currOrder.TotalPrice, &currOrder.OrderDate, &currOrder.OrderTime,
			&currOrder.OrderState, &currOrder.OrderCode, &currOrder.HeadquartersName, &currOrder.OrderStatusType)
		if err != nil {
			log.Printf("Error %s when creating DB\n", err)
			return nil, err
		}
		resDetail, err := repoR.db.Query(mysqlSelectOrderDetailQuery, currOrder.PayerId, currOrder.OrderCode)
		if err != nil {
			log.Printf("Error %s when creating DB\n", err)
			return nil, err
		}
		defer resDetail.Close()

		for resDetail.Next() {
			currDetailItem := &product.ItemDetailResponse{}

			_ = resDetail.Scan(&currDetailItem.Quantity, &currDetailItem.UnitPrice, &currDetailItem.ItemId, &currDetailItem.ItemTitle, &currDetailItem.CategoryId, &currDetailItem.ImageName)

			lastOrderDetail = append(lastOrderDetail, currDetailItem)
		}
		currOrder.ListaItems = lastOrderDetail

		lastOrders = append(lastOrders, currOrder)
	}

	return lastOrders, nil
}
func (repoR *mysqlProductsRepository) GetOrderByOrderCode(branchId uint32, orderCode uint64, headquarterId uint32) (*product.OrdersRequest, error) {
	res := repoR.db.QueryRow(mysqlSelectSingleOrderQuery, branchId, headquarterId, branchId, headquarterId, branchId, headquarterId, branchId, headquarterId, branchId, orderCode)
	var lastOrderDetail []*product.ItemDetailResponse
	var currOrder = &product.OrdersRequest{}
	err := res.Scan(&currOrder.PayerId, &currOrder.Email, &currOrder.Name, &currOrder.Surname,
		&currOrder.StreetName, &currOrder.StreetNumber, &currOrder.PhoneNumber, &currOrder.MethodOfDelivery,
		&currOrder.PaymentMethodOptions, &currOrder.TotalPrice, &currOrder.OrderDate, &currOrder.OrderTime,
		&currOrder.OrderState, &currOrder.OrderCode, &currOrder.HeadquartersName, &currOrder.OrderStatusType)
	if err != nil && err != sql.ErrNoRows {
		log.Printf("Error %s when calling DB\n", err)
		return nil, err
	}
	resDetail, err := repoR.db.Query(mysqlSelectSingleDetailOrderQuery, currOrder.PayerId, currOrder.OrderCode)
	if err != nil {
		log.Printf("Error %s when calling DB\n", err)
		return nil, err
	}
	defer resDetail.Close()

	for resDetail.Next() {
		currDetailItem := &product.ItemDetailResponse{}

		_ = resDetail.Scan(&currDetailItem.Quantity, &currDetailItem.UnitPrice, &currDetailItem.ItemId, &currDetailItem.ItemTitle, &currDetailItem.CategoryId, &currDetailItem.ImageName)

		lastOrderDetail = append(lastOrderDetail, currDetailItem)
	}
	currOrder.ListaItems = lastOrderDetail

	return currOrder, nil
}
