package handler

import (
	"context"
	"fabrikapps/payment-products-code/pkg/errors"
	"log"
	"time"

	"google.golang.org/grpc"

	product_model "fabrikapps/payment-products-code/pkg/domain/product"
	"fabrikapps/payment-products-code/pkg/infrastrucure/delivery/grpc/paymentProductspb"
	app_get_service "fabrikapps/payment-products-code/pkg/service"
)

type service struct {
	serv app_get_service.Service
}

// NewProductListServerGrpc ...
func NewInsertProductsServerGrpc(gserver *grpc.Server, insertserv app_get_service.Service) {
	tempService := &service{
		serv: insertserv,
	}
	paymentProductspb.RegisterPaymentProductsServer(gserver, tempService)
}

func (s *service) InsertProductsPurchased(ctx context.Context, reqPaymentToIns *paymentProductspb.PaymentToProcess) (*paymentProductspb.ItemStatusResponse, error) {
	ctx, cancelfunc := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancelfunc()
	var modelListItems []*product_model.Item
	for _, item := range reqPaymentToIns.ListaItems {
		tempModelItem := &product_model.Item{
			Title:       item.Title,
			Description: item.Description,
			Quantity:    item.Quantity,
			CurrencyId:  item.CurrencyId,
			UnitPrice:   item.UnitPrice,
			ItemId:      item.ItemId,
			CategoryId:  item.CategoryId,
		}
		modelListItems = append(modelListItems, tempModelItem)
	}
	phoneToIns := &product_model.Phone{
		AreaCode: reqPaymentToIns.Payer.Phone.AreaCode,
		Number:   reqPaymentToIns.Payer.Phone.Number,
	}
	streetToIns := &product_model.Address{
		ZipCode:      reqPaymentToIns.Payer.Address.ZipCode,
		StreetName:   reqPaymentToIns.Payer.Address.StreetName,
		StreetNumber: reqPaymentToIns.Payer.Address.StreetNumber,
	}
	payerToIns := &product_model.Payer{
		Email:   reqPaymentToIns.Payer.Email,
		Name:    reqPaymentToIns.Payer.Name,
		Surname: reqPaymentToIns.Payer.Surname,
		Phone:   phoneToIns,
		Address: streetToIns,
	}
	paymentToInsert := &product_model.PaymentToProcess{
		ListaItems:           modelListItems,
		Payer:                payerToIns,
		StatementDescriptor:  reqPaymentToIns.StatementDescriptor,
		AdditionalInfo:       reqPaymentToIns.AdditionalInfo,
		MethodOfDelivery:     reqPaymentToIns.MethodOfDelivery,
		PaymentMethodOptions: reqPaymentToIns.PaymentMethodOptions,
		TotalPrice:           reqPaymentToIns.TotalPrice,
		HeadquartersId:       reqPaymentToIns.HeadquarterId,
		BranchId:             reqPaymentToIns.BranchId,
		OrderMpGenerated:     reqPaymentToIns.OrderMpGenerated,
	}
	statusSuccessfulOrNo, err, orderCode := s.serv.InsertPaymentService(paymentToInsert)
	response := &paymentProductspb.ItemStatusResponse{
		StatusSuccessful: statusSuccessfulOrNo,
		Error:            "No errors",
		OrderCode:        orderCode,
	}
	if err != nil {
		log.Println(err.Error())
		return response, err
	}
	if statusSuccessfulOrNo == false {
		return response, errors.ErrNoUpdatedItem
	}
	return response, nil
}
func (s *service) ChangeOrderState(ctx context.Context, reqOrderToChange *paymentProductspb.OrderToChangeRequest) (*paymentProductspb.ItemStatusResponse, error) {
	ctx, cancelfunc := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancelfunc()
	statusSuccessfulOrNo, err := s.serv.ChangeOrderPaymentService(reqOrderToChange.NewStateOrder, reqOrderToChange.OrderCode)
	response := &paymentProductspb.ItemStatusResponse{
		StatusSuccessful: false,
		Error:            "Some error",
		OrderCode:        0,
	}
	if err != nil {
		log.Println(err.Error())
		return response, err
	}
	if statusSuccessfulOrNo == false {
		return response, errors.ErrNoUpdatedItem
	}
	response = &paymentProductspb.ItemStatusResponse{
		StatusSuccessful: statusSuccessfulOrNo,
		Error:            "No errors",
		OrderCode:        reqOrderToChange.OrderCode,
	}
	return response, nil
}
func (s *service) CancelOrderStatus(ctx context.Context, reqOrderToCancel *paymentProductspb.OrderToCancelRequest) (*paymentProductspb.ItemStatusResponse, error) {
	ctx, cancelfunc := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancelfunc()
	statusSuccessfulOrNo, err := s.serv.CancelOrderPaymentService(reqOrderToCancel.OrderCode)
	response := &paymentProductspb.ItemStatusResponse{
		StatusSuccessful: false,
		Error:            "Some error",
		OrderCode:        0,
	}
	if err != nil {
		log.Println(err.Error())
		return response, err
	}
	if statusSuccessfulOrNo == false {
		return response, errors.ErrNoUpdatedItem
	}
	response = &paymentProductspb.ItemStatusResponse{
		StatusSuccessful: statusSuccessfulOrNo,
		Error:            "No errors",
		OrderCode:        reqOrderToCancel.OrderCode,
	}
	return response, nil
}

func (s *service) GetLastOrders(request *paymentProductspb.GetLastOrdersRequest, stream paymentProductspb.PaymentProducts_GetLastOrdersServer) error {
	for {
		var orderList []*paymentProductspb.OrdersToProcess
		ordersDB, err := s.serv.SelectLastOrdersService(request.BranchId, request.HeadquarterId)
		if err != nil {
			log.Println(err.Error())
			return err
		}
		for _, element := range ordersDB {
			var itemToOrderList []*paymentProductspb.Item
			for _, itemElement := range element.ListaItems {
				item := &paymentProductspb.Item{
					ItemId:        itemElement.ItemId,
					CategoryId:    itemElement.CategoryId,
					Title:         itemElement.ItemTitle,
					Quantity:      itemElement.Quantity,
					UnitPrice:     itemElement.UnitPrice,
					HeadquarterId: request.HeadquarterId,
					BranchId:      request.BranchId,
					ImageName:     itemElement.ImageName,
				}
				itemToOrderList = append(itemToOrderList, item)
			}
			products := &paymentProductspb.OrdersToProcess{
				MethodOfDelivery:     element.MethodOfDelivery,
				PaymentMethodOptions: element.PaymentMethodOptions,
				TotalPrice:           element.TotalPrice,
				OrderDate:            element.OrderDate,
				OrderTime:            element.OrderTime,
				OrderCode:            element.OrderCode,
				OrderStatus:          element.OrderState,
				Payer: &paymentProductspb.Payer{
					Email:   element.Email,
					Name:    element.Name,
					Surname: element.Surname,
					Phone: &paymentProductspb.Phone{
						Number: element.PhoneNumber,
					},
					Address: &paymentProductspb.Address{
						StreetName:   element.StreetName,
						StreetNumber: element.StreetNumber,
					},
				},
				ListaItems:       itemToOrderList,
				HeadquartersName: element.HeadquartersName,
				OrderStatusType:  element.OrderStatusType,
			}
			orderList = append(orderList, products)
		}
		res := &paymentProductspb.GetLastOrdersResponse{
			ListaOrders: orderList,
		}
		stream.Send(res)
		for i := 0; i < 10; i++ {
			if stream.Context().Err() == context.Canceled || stream.Context().Err() == context.DeadlineExceeded {
				log.Print("Context is cancelled")
				return context.Canceled
			}
			time.Sleep(1 * time.Second)
		}
	}
	//return nil
}
func (s *service) GetLastOrderById(request *paymentProductspb.GetLastOrderByIdRequest, stream paymentProductspb.PaymentProducts_GetLastOrderByIdServer) error {
	for {
		orderDB, err := s.serv.GetOrderByOrderCodeService(request.BranchId, request.OrderCode, request.HeadquarterId)
		if err != nil {
			log.Println(err.Error())
			return err
		}
		var itemToOrderList []*paymentProductspb.Item
		for _, itemElement := range orderDB.ListaItems {
			item := &paymentProductspb.Item{
				ItemId:        itemElement.ItemId,
				CategoryId:    itemElement.CategoryId,
				Title:         itemElement.ItemTitle,
				Quantity:      itemElement.Quantity,
				UnitPrice:     itemElement.UnitPrice,
				HeadquarterId: request.HeadquarterId,
				BranchId:      request.BranchId,
				ImageName:     itemElement.ImageName,
			}
			itemToOrderList = append(itemToOrderList, item)
		}
		products := &paymentProductspb.OrdersToProcess{
			MethodOfDelivery:     orderDB.MethodOfDelivery,
			PaymentMethodOptions: orderDB.PaymentMethodOptions,
			TotalPrice:           orderDB.TotalPrice,
			OrderDate:            orderDB.OrderDate,
			OrderTime:            orderDB.OrderTime,
			OrderCode:            orderDB.OrderCode,
			OrderStatus:          orderDB.OrderState,
			Payer: &paymentProductspb.Payer{
				Email:   orderDB.Email,
				Name:    orderDB.Name,
				Surname: orderDB.Surname,
				Phone: &paymentProductspb.Phone{
					Number: orderDB.PhoneNumber,
				},
				Address: &paymentProductspb.Address{
					StreetName:   orderDB.StreetName,
					StreetNumber: orderDB.StreetNumber,
				},
			},
			ListaItems:       itemToOrderList,
			HeadquartersName: orderDB.HeadquartersName,
			OrderStatusType:  orderDB.OrderStatusType,
		}

		res := &paymentProductspb.GetLastOrderByIdResponse{
			OrderById: products,
		}
		stream.Send(res)
		for i := 0; i < 10; i++ {
			if stream.Context().Err() == context.Canceled || stream.Context().Err() == context.DeadlineExceeded {
				log.Print("Context is cancelled")
				return context.Canceled
			}
			time.Sleep(1 * time.Second)
		}
	}
	//return nil
}
