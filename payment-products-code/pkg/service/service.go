package service

import (
	"fabrikapps/payment-products-code/pkg/domain/product"
	"log"
)

type Service interface {
	InsertPaymentService(reqPaymentToInsert *product.PaymentToProcess) (bool, error, uint64)
	ChangeOrderPaymentService(reqNewStateOrder uint64, reqOrderPaymentToChange uint64) (bool, error)
	CancelOrderPaymentService(reqOrderPaymentToChange uint64) (bool, error)
	SelectLastOrdersService(branchId uint32, headquarterId uint32) ([]*product.OrdersRequest, error)
	GetOrderByOrderCodeService(branchId uint32, orderCode uint64, headquarterId uint32) (*product.OrdersRequest, error)
}

type service struct {
	getRepo product.Repository
}

func NewService(p product.Repository) Service {
	return &service{p}
}

func (s *service) InsertPaymentService(reqPaymentToInsert *product.PaymentToProcess) (bool, error, uint64) {
	valueSuccessful, err, orderCode := s.getRepo.InsertPaymentRepo(reqPaymentToInsert)
	if err != nil {
		log.Printf("Error %s when calling the setProduct\n", err)
		return valueSuccessful, err, orderCode
	}
	return valueSuccessful, nil, orderCode
}
func (s *service) ChangeOrderPaymentService(reqNewStateOrder uint64, reqOrderPaymentToChange uint64) (bool, error) {
	valueSuccessful, err := s.getRepo.ChangeOrderPaymentRepo(reqNewStateOrder, reqOrderPaymentToChange)
	if err != nil {
		log.Printf("Error %s when calling the setProduct\n", err)
		return valueSuccessful, err
	}
	return valueSuccessful, nil
}
func (s *service) CancelOrderPaymentService(reqOrderPaymentToChange uint64) (bool, error) {
	valueSuccessful, err := s.getRepo.CancelOrderPaymentRepo(reqOrderPaymentToChange)
	if err != nil {
		log.Printf("Error %s when calling the setProduct\n", err)
		return valueSuccessful, err
	}
	return valueSuccessful, nil
}
func (s *service) SelectLastOrdersService(branchId uint32, headquarterId uint32) ([]*product.OrdersRequest, error) {
	lastOrders, err := s.getRepo.SelectLastOrdersRepo(branchId, headquarterId)
	if err != nil {
		log.Printf("Error %s when calling the setProduct\n", err)
		return nil, err
	}
	return lastOrders, nil
}
func (s *service) GetOrderByOrderCodeService(branchId uint32, orderCode uint64, headquarterId uint32) (*product.OrdersRequest, error) {
	lastOrder, err := s.getRepo.GetOrderByOrderCode(branchId, orderCode, headquarterId)
	if err != nil {
		log.Printf("Error %s when calling the setProduct\n", err)
		return nil, err
	}
	return lastOrder, nil
}
