package product

// Repository provides access to role repository
type Repository interface {
	InsertPaymentRepo(reqPaymentToInsert *PaymentToProcess) (bool, error, uint64)
	ChangeOrderPaymentRepo(reqNewStateOrder uint64, reqOrderPaymentToChange uint64) (bool, error)
	CancelOrderPaymentRepo(reqOrderPaymentToChange uint64) (bool, error)
	SelectLastOrdersRepo(branchId uint32, headquarterId uint32) ([]*OrdersRequest, error)
	GetOrderByOrderCode(branchId uint32, orderCode uint64, headquarterId uint32) (*OrdersRequest, error)
}
