package product

type PaymentToProcess struct {
	ListaItems           []*Item
	Payer                *Payer
	StatementDescriptor  string
	AdditionalInfo       string
	MethodOfDelivery     string
	PaymentMethodOptions string
	TotalPrice           float32
	HeadquartersId       uint32
	BranchId             uint32
	OrderMpGenerated     uint32
}

type Item struct {
	Title       string
	Description string
	Quantity    uint32
	CurrencyId  string
	UnitPrice   float32
	ItemId      uint32
	CategoryId  uint32
}
type ItemDetailResponse struct {
	Quantity   uint32
	UnitPrice  float32
	ItemId     uint32
	CategoryId uint32
	ItemTitle  string
	ImageName  string
}

type Payer struct {
	Email   string
	Name    string
	Surname string
	Phone   *Phone
	Address *Address
}
type Phone struct {
	AreaCode string
	Number   string
}
type Address struct {
	ZipCode      string
	StreetName   string
	StreetNumber uint32
}

type ProductList struct {
	ListOfItems []*Item
}

type OrdersRequest struct {
	ListaItems   []*ItemDetailResponse
	StreetName   string
	StreetNumber uint32
	PhoneNumber  string
	Email        string
	Name         string
	Surname      string
	OrderState   uint32
	// AdditionalInfo       string
	MethodOfDelivery     string
	PaymentMethodOptions string
	TotalPrice           float32
	OrderDate            string
	OrderTime            string
	OrderCode            uint64
	PayerId              uint64
	HeadquartersName     string
	OrderStatusType      string
}

func (o *OrdersRequest) GetListaItems() []*ItemDetailResponse {
	return o.ListaItems
}
