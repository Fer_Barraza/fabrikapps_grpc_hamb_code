package main

import (
	"fabrikapps/payment-products-code/pkg/config/database"
	"fabrikapps/payment-products-code/pkg/infrastrucure/delivery/handler"
	"fabrikapps/payment-products-code/pkg/service"

	"fabrikapps/payment-products-code/pkg/infrastrucure/storage/mysql"
	"fmt"
	"log"
	"net"
	"os"

	"google.golang.org/grpc"
)

func main() {
	log.Println("iniciando grpc server")
	databaseImplementation := os.Args[1]
	var userdb = os.Getenv("MYSQLUSERNAME")
	var passdb = os.Getenv("MYSQLPASS")
	// var userdb = "root"
	// var passdb = "mari 35 caballos"
	// databaseImplementation := "localhost" //change in local dev
	dbConn, err := database.GetConnection(databaseImplementation, userdb, passdb)
	if err != nil {
		fmt.Println(err)
	}
	defer dbConn.Close()

	lis, err := net.Listen("tcp", ":50052")
	if err != nil {
		log.Fatalf("Failed to listen: %v", err)
	}

	rp := mysql.NewMysqlProductsRepository(dbConn)
	ss := service.NewService(rp)

	grpcSrv := grpc.NewServer()
	handler.NewInsertProductsServerGrpc(grpcSrv, ss)

	log.Println("Listen on :50052")
	if err := grpcSrv.Serve(lis); err != nil {
		log.Fatalf("Failed to serve: %v", err)
	}

}
