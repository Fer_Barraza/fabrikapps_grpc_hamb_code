#! /bin/bash

echo "	Ingrese image name"

read var_image_name

echo "	Ingrese image tag"

read var_image_tag

echo "	Creating docker image"

sudo docker build -t $var_image_name:$var_image_tag .

# IMAGE_ID=$(sudo docker images --filter=reference=$var_image_name --format "{{.ID}}")

# echo "	Image Id = $IMAGE_ID"

# sudo docker tag $IMAGE_ID $var_image_name:$var_image_tag

echo "	Pushing docker image"

sudo docker push $var_image_name:$var_image_tag