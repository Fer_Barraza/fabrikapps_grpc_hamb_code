package service

import (
	"fabrikapps/item-products-hamb-code/pkg/domain/product"
	"log"
)

type Service interface {
	GetProductListCatService(reqCatId uint32, reqBranchId uint32, reqHeadquarterId uint32) ([]*product.Item, error)
	SetProductService(reqItemToRepl *product.Item) (bool, error)
	GetCategoriesListService(reqBranchId uint32, reqHeadquarterId uint32) ([]*product.Categorie, error)
}

type service struct {
	getRepo product.Repository
}

func NewService(p product.Repository) Service {
	return &service{p}
}

func (s *service) GetProductListCatService(reqCatId uint32, reqBranchId uint32, reqHeadquarterId uint32) ([]*product.Item, error) {
	products, err := s.getRepo.GetProductListCat(reqCatId, reqBranchId, reqHeadquarterId)
	if err != nil {
		log.Printf("Error %s when calling the getProductListCat\n", err)
		return nil, nil
	}
	return products, nil
}
func (s *service) SetProductService(reqItemToRepl *product.Item) (bool, error) {
	valueSuccessful, err := s.getRepo.SetProductRepo(reqItemToRepl)
	if err != nil {
		log.Printf("Error %s when calling the setProduct\n", err)
		return valueSuccessful, err
	}
	return valueSuccessful, nil
}
func (s *service) GetCategoriesListService(reqBranchId uint32, reqHeadquarterId uint32) ([]*product.Categorie, error) {
	categories, err := s.getRepo.GetCatList(reqBranchId, reqHeadquarterId)
	if err != nil {
		log.Printf("Error %s when calling the getProductListCat\n", err)
		return nil, nil
	}
	return categories, nil
}
