package mysql

import (
	"database/sql"
	"fabrikapps/item-products-hamb-code/pkg/domain/product"
	"log"
)

const (
	mysqlGetProductListCat = "SELECT c.CategoriasMenuNombre, i.Item_Id, i.Item_CategoriasMenu_id, i.Item_Title, i.Item_ImageName, i.Item_Description, i.Item_Unit_Price FROM payment_checkout_db4.Item i INNER JOIN payment_checkout_db4.Category c ON i.Item_CategoriasMenu_id = c.CategoriasMenu_id WHERE i.Item_CategoriasMenu_id = ? AND i.Branch_has_Headquarters_Branch_Branch_Id = ? AND i.Branch_has_Headquarters_Headquarters_Headquarters_Id = ?;"
	//mysqlSetProductQuery                         = "UPDATE itemproductsdb.TipoItem SET TipoItem_NombreItem=?, TipoItem_Descripcion=?, TipoItem_Price=? WHERE TipoMenuItem_id=?;"
	mysqlSetProductQuery = `UPDATE payment_checkout_db4.Item SET Item_Title = ?, Item_Description = ?, Item_Unit_Price = ? WHERE Item_Id = ?
	AND Branch_has_Headquarters_Branch_Branch_Id = ? AND Branch_has_Headquarters_Headquarters_Headquarters_Id = ?;`
	mysqlGetProductListFromCatAndBranchIdAndHqId = `SELECT c.CategoriasMenuNombre, 
	i.Item_Id, i.Item_CategoriasMenu_id, i.Item_Title, i.Item_ImageName, i.Item_Description, i.Item_Unit_Price, i.Item_Currency_id
	FROM payment_checkout_db4.Item i INNER JOIN payment_checkout_db4.Category c ON i.Item_CategoriasMenu_id = c.CategoriasMenu_id 
	WHERE i.Item_CategoriasMenu_id = ? 
	AND i.Branch_has_Headquarters_Branch_Branch_Id = ? 
	AND i.Branch_has_Headquarters_Headquarters_Headquarters_Id = ?;`
	mysqlGetCategoriesListFromBranchAndHeadquarter = `SELECT DISTINCT CategoriasMenu_id, CategoriasMenuNombre, Categorias_UrlImage
	FROM payment_checkout_db4.Category c 
	INNER JOIN payment_checkout_db4.Item i 
	ON i.Item_CategoriasMenu_id = c.CategoriasMenu_id 
	WHERE  i.Branch_has_Headquarters_Branch_Branch_Id =? 
	AND i.Branch_has_Headquarters_Headquarters_Headquarters_Id = ?;`
)

// Storage keeps data in db
type mysqlProductsRepository struct {
	db *sql.DB
}

func NewMysqlProductsRepository(db *sql.DB) product.Repository {
	return &mysqlProductsRepository{db}
}

// GetProductListCat get the given Product list to the repository
func (reposincampos *mysqlProductsRepository) GetProductListCat(reqCatId uint32, reqBranchId uint32, reqHeadquarterId uint32) ([]*product.Item, error) {

	res, err := reposincampos.db.Query(mysqlGetProductListFromCatAndBranchIdAndHqId, reqCatId, reqBranchId, reqHeadquarterId)
	if err != nil {
		log.Printf("Error %s when creating DB\n", err)
		return nil, err
	}

	var products []*product.Item

	for res.Next() {
		product := &product.Item{}

		_ = res.Scan(&product.Category, &product.ItemId, &product.CategoryId, &product.Title, &product.ImageName,
			&product.Description, &product.UnitPrice, &product.CurrencyId)

		products = append(products, product)
	}

	return products, nil
}

func (repoR *mysqlProductsRepository) SetProductRepo(reqItemToRepl *product.Item) (bool, error) {
	_, err := repoR.db.Exec(mysqlSetProductQuery, reqItemToRepl.Title, reqItemToRepl.Description, reqItemToRepl.UnitPrice, reqItemToRepl.ItemId, reqItemToRepl.BranchId, reqItemToRepl.HeadquarterId)
	if err != nil {
		log.Printf("Error %s when creating DB\n", err)
		return false, err
	}
	return true, nil
}
func (reposincampos *mysqlProductsRepository) GetCatList(reqBranchId uint32, reqHeadquarterId uint32) ([]*product.Categorie, error) {
	res, err := reposincampos.db.Query(mysqlGetCategoriesListFromBranchAndHeadquarter, reqBranchId, reqHeadquarterId)
	if err != nil {
		log.Printf("Error %s when creating DB\n", err)
		return nil, err
	}
	var categories []*product.Categorie
	for res.Next() {
		categorie := &product.Categorie{}
		_ = res.Scan(&categorie.CategorieId, &categorie.CatName, &categorie.CatUrlImg)
		categories = append(categories, categorie)
	}
	return categories, nil
}
