package handler

import (
	"context"
	"fabrikapps/item-products-hamb-code/pkg/errors"
	"log"
	"time"

	"google.golang.org/grpc"

	product_model "fabrikapps/item-products-hamb-code/pkg/domain/product"
	"fabrikapps/item-products-hamb-code/pkg/infrastrucure/delivery/grpc/itemProductspb"
	app_get_service "fabrikapps/item-products-hamb-code/pkg/service"
)

type service struct {
	serv app_get_service.Service
}

// NewProductListServerGrpc ...
func NewProductListServerGrpc(gserver *grpc.Server, getserv app_get_service.Service) {

	asdfafd := &service{
		serv: getserv,
	}

	itemProductspb.RegisterProductoStockServer(gserver, asdfafd)
}

//GetProductsList ....
func (s *service) GetProductsList(ctx context.Context, req *itemProductspb.CategoryIdRequest) (*itemProductspb.ItemListResponse, error) {
	_, cancelfunc := context.WithTimeout(ctx, 5*time.Second)
	defer cancelfunc()

	var prList []*product_model.Item
	prList, err := s.serv.GetProductListCatService(req.ReqCategoryId, req.ReqBranchId, req.ReqHeadquarterId)
	if err != nil {
		log.Println(err.Error())
		return nil, err
	}
	if prList == nil {
		return nil, errors.ErrNoListItems
	}

	var products []*itemProductspb.Item

	for _, element := range prList {
		product := &itemProductspb.Item{
			CategoryId:    element.CategoryId,
			ItemId:        element.ItemId,
			Title:         element.Title,
			ImageName:     element.ImageName,
			Description:   element.Description,
			UnitPrice:     element.UnitPrice,
			CurrencyId:    element.CurrencyId,
			HeadquarterId: req.ReqHeadquarterId,
			BranchId:      req.ReqBranchId,
			Category:      element.Category,
		}

		products = append(products, product)
	}

	if products == nil {
		return nil, errors.ErrNoListItemsTemp
	}

	response := &itemProductspb.ItemListResponse{
		ListaItems: products,
	}
	return response, nil
}

func (s *service) SetProduct(ctx context.Context, reqItemToRepl *itemProductspb.Item) (*itemProductspb.ItemStatusResponse, error) {
	_, cancelfunc := context.WithTimeout(ctx, 5*time.Second)
	defer cancelfunc()

	reqItem := reqItemToRepl
	//var prItemToRepl *product_model.Item

	prItemToRepl := &product_model.Item{
		CategoryId:    reqItem.CategoryId,
		ItemId:        reqItem.ItemId,
		Title:         reqItem.Title,
		ImageName:     reqItem.ImageName,
		Description:   reqItem.Description,
		UnitPrice:     reqItem.UnitPrice,
		CurrencyId:    reqItem.CurrencyId,
		HeadquarterId: reqItem.HeadquarterId,
		BranchId:      reqItem.BranchId,
	}

	statusSuccessfulOrNo, err := s.serv.SetProductService(prItemToRepl)
	if err != nil {
		log.Println(err.Error())
		return nil, err
	}
	if statusSuccessfulOrNo == false {
		return nil, errors.ErrNoUpdatedItem
	}

	response := &itemProductspb.ItemStatusResponse{
		StatusSuccessful: statusSuccessfulOrNo,
	}
	return response, nil
}

func (s *service) GetCategoriesList(ctx context.Context, req *itemProductspb.GetCategoriesListRequest) (*itemProductspb.GetCategoriesListResponse, error) {
	_, cancelfunc := context.WithTimeout(ctx, 5*time.Second)
	defer cancelfunc()

	var catList []*product_model.Categorie
	catList, err := s.serv.GetCategoriesListService(req.ReqBranchId, req.ReqHeadquarterId)
	if err != nil {
		log.Println(err.Error())
		return nil, err
	}
	if catList == nil {
		return nil, errors.ErrNoListItems
	}

	var categories []*itemProductspb.Category

	for _, element := range catList {
		categorie := &itemProductspb.Category{
			ResCategoriesId:         element.CategorieId,
			ResCategoriesNombreId:   element.CatName,
			ResCategoriesUrlimageId: element.CatUrlImg,
		}

		categories = append(categories, categorie)
	}

	if categories == nil {
		return nil, errors.ErrNoListItemsTemp
	}

	response := &itemProductspb.GetCategoriesListResponse{
		ListCategories: categories,
	}
	return response, nil
}
