package product

type Categorie struct {
	CategorieId uint32
	CatName     string
	CatUrlImg   string
}
type Item struct {
	// Category    string  `db:"CategoriasMenuNombre"`
	// ItemId      uint32  `db:"TipoMenuItem_id"`
	// CategoryId  uint32  `db:"TipoItem_categoriasMenuId"`
	// NombreItem  string  `db:"TipoItem_NombreItem"`
	// ImageName   string  `db:"TipoItem_ImageName"`
	// Descripcion string  `db:"TipoItem_Descripcion"`
	// ItemPrice   float32 `db:"TipoItem_Price"`
	Title         string
	Category      string
	Description   string
	Quantity      uint32
	CurrencyId    string
	UnitPrice     float32
	ItemId        uint32
	CategoryId    uint32
	HeadquarterId uint32
	BranchId      uint32
	ImageName     string
}

// type ProductList struct {
// 	ListOfItems []*Item
// }
