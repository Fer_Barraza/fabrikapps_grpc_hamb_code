package product

// Repository provides access to role repository
type Repository interface {
	GetProductListCat(reqCatId uint32, reqBranchId uint32, reqHeadquarterId uint32) ([]*Item, error)
	SetProductRepo(reqItemToRepl *Item) (bool, error)
	GetCatList(reqCreqBranchId uint32, reqHeadquarterId uint32) ([]*Categorie, error)
}
