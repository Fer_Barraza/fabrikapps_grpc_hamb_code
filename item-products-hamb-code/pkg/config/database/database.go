package database

import (
	"database/sql"
	"fmt"
	"log"

	"fabrikapps/item-products-hamb-code/pkg/errors"

	_ "github.com/go-sql-driver/mysql"
)

var (
	databaseConnection *sql.DB
)

func GetConnection(databaseImplementation string, userdb string, passdb string) (*sql.DB, error) {
	var err error
	connectionString := fmt.Sprintf("%s:%s@tcp(%s:3306)/payment_checkout_db4", userdb, passdb, databaseImplementation)
	databaseConnection, err = sql.Open("mysql", connectionString)
	if err != nil {
		log.Fatalf("Can not connect to database: %v", err)
		err = errors.ErrSQLConnect
		return nil, err

	}
	err = databaseConnection.Ping()
	if err != nil {
		log.Fatalf("Database error: %v", err)
		err = errors.ErrSQLConnect
		return nil, err
	}

	return databaseConnection, err

}
