package main

import (
	"fabrikapps/item-products-hamb-code/pkg/config/database"
	"fabrikapps/item-products-hamb-code/pkg/infrastrucure/delivery/handler"
	"fabrikapps/item-products-hamb-code/pkg/service"

	"fabrikapps/item-products-hamb-code/pkg/infrastrucure/storage/mysql"
	"fmt"
	"log"
	"net"
	"os"

	"google.golang.org/grpc"
)

func main() {
	log.Println("iniciando grpc server")
	databaseImplementation := os.Args[1]
	var userdb = os.Getenv("MYSQLUSERNAME")
	var passdb = os.Getenv("MYSQLPASS")
	//databaseImplementation := "localhost" //change in local dev
	dbConn, err := database.GetConnection(databaseImplementation, userdb, passdb)
	if err != nil {
		fmt.Println(err)
	}
	defer dbConn.Close()

	lis, err := net.Listen("tcp", ":50057")
	if err != nil {
		log.Fatalf("Failed to listen: %v", err)
	}

	rp := mysql.NewMysqlProductsRepository(dbConn)
	ss := service.NewService(rp)

	grpc_srv := grpc.NewServer()
	handler.NewProductListServerGrpc(grpc_srv, ss)

	log.Println("Listen on :50057")
	if err := grpc_srv.Serve(lis); err != nil {
		log.Fatalf("Failed to serve: %v", err)
	}

}
