-- MySQL dump 10.13  Distrib 8.0.23, for Linux (x86_64)
--
-- Host: localhost    Database: itemproductsdb
-- ------------------------------------------------------
-- Server version	8.0.23-0ubuntu0.20.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Branch`
--

DROP TABLE IF EXISTS `Branch`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Branch` (
  `Branch_Id` mediumint unsigned NOT NULL AUTO_INCREMENT,
  `Branch_Name` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`Branch_Id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Branch`
--

LOCK TABLES `Branch` WRITE;
/*!40000 ALTER TABLE `Branch` DISABLE KEYS */;
INSERT INTO `Branch` VALUES (1,'Fabrikapps');
/*!40000 ALTER TABLE `Branch` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CategoriasMenu`
--

DROP TABLE IF EXISTS `CategoriasMenu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `CategoriasMenu` (
  `CategoriasMenu_id` tinyint unsigned NOT NULL AUTO_INCREMENT,
  `CategoriasMenuNombre` varchar(45) NOT NULL,
  PRIMARY KEY (`CategoriasMenu_id`),
  UNIQUE KEY `CategoriasMenu_id_UNIQUE` (`CategoriasMenu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CategoriasMenu`
--

LOCK TABLES `CategoriasMenu` WRITE;
/*!40000 ALTER TABLE `CategoriasMenu` DISABLE KEYS */;
INSERT INTO `CategoriasMenu` VALUES (1,'Hamburguesas'),(2,'Papas'),(3,'Cervezas'),(4,'Gaseosas'),(5,'Helado'),(6,'Panchos'),(7,'Otros'),(8,'Promos');
/*!40000 ALTER TABLE `CategoriasMenu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `TipoItem`
--

DROP TABLE IF EXISTS `TipoItem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `TipoItem` (
  `TipoMenuItem_id` mediumint unsigned NOT NULL AUTO_INCREMENT,
  `TipoItem_categoriasMenuId` tinyint unsigned NOT NULL,
  `TipoItem_NombreItem` varchar(45) NOT NULL,
  `TipoItem_ImageName` varchar(90) DEFAULT NULL,
  `TipoItem_Descripcion` varchar(45) DEFAULT NULL,
  `TipoItem_Price` float DEFAULT NULL,
  `TipoItem_BranchId` mediumint unsigned NOT NULL,
  PRIMARY KEY (`TipoMenuItem_id`),
  UNIQUE KEY `nombreItem_UNIQUE` (`TipoItem_NombreItem`),
  UNIQUE KEY `TipoMenuItem_id_UNIQUE` (`TipoMenuItem_id`),
  KEY `fk_tipoItem_categoriasMenu_idx` (`TipoItem_categoriasMenuId`),
  KEY `TipoItem_FK` (`TipoItem_BranchId`),
  CONSTRAINT `fk_tipoItem_categoriasMenu` FOREIGN KEY (`TipoItem_categoriasMenuId`) REFERENCES `CategoriasMenu` (`CategoriasMenu_id`),
  CONSTRAINT `TipoItem_FK` FOREIGN KEY (`TipoItem_BranchId`) REFERENCES `Branch` (`Branch_Id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `TipoItem`
--

LOCK TABLES `TipoItem` WRITE;
/*!40000 ALTER TABLE `TipoItem` DISABLE KEYS */;
INSERT INTO `TipoItem` VALUES (1,1,'Matadora','hamburguesa_1.jpg','Lechuga y tomate',250,1),(2,1,'Poderosa','Mexicana.jpg','Pepino',300,1),(3,1,'Terminator','Sin-titulo-1.jpg','Cheddar y bacon',560,1),(4,1,'Explosion de Cheddar','D03.png','Extra cheddar',600,1),(5,1,'La Simple ','D01.png','Tomate y queso',130,1),(100,2,'La Combinada','fries_01.jpg','Cheddar y bacon',120,1),(101,2,'Turbulencia','muzzarelitas.jpg','Cheddar, bacon y aderezos',150,1);
/*!40000 ALTER TABLE `TipoItem` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-03-10 22:20:53
