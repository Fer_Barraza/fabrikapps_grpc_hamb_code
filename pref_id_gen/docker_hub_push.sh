#! /bin/bash

echo "	Ingrese image name"

read var_image_name

echo "	Ingrese image tag"

read var_image_tag

sudo docker build -t ferbarraza/$var_image_name .

IMAGE_ID=$(sudo docker images --filter=reference=$var_image_name --format "{{.ID}}")

sudo docker tag $IMAGE_ID $var_image_name:$var_image_tag

sudo docker push ferbarraza/$var_image_name:$var_image_tag