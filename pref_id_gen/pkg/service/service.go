package service

import (
	"fabrikapps/pref_id_gen/pkg/domain/product"
	"fabrikapps/pref_id_gen/pkg/errors"
	"log"
)

type Service interface {
	PrefIdGenService(args []byte, branchId int32) (string, error)
}

type service struct {
	getRepo product.Repository
}

func NewService(p product.Repository) Service {
	return &service{p}
}

func (s *service) PrefIdGenService(args []byte, branchId int32) (string, error) {
	accessToken := ""
	switch branchId {
	case 1:
		accessToken = "TEST-4561833546465531-111023-0fec7fec733cdb0067ed1ab1ec9209b1-236976484"
	// case 2:
	// 	accessToken = "TEST-4561833546465531-111023-0fec7fec733cdb0067ed1ab1ec9209b1-236976484"
	default:
		err := errors.ErrInvalidBranch
		//log.Fatalf("Can not process the branchId: %v", err)
		return "", err
	}

	jsonByte, err := s.getRepo.PrefIdGenRepo(args, accessToken)
	if err != nil {
		log.Printf("Error %s when calling the setProduct\n", err)
		return "", err
	}
	return jsonByte, err
}
