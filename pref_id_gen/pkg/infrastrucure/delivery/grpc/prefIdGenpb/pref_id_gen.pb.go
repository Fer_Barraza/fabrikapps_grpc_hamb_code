// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.25.0-devel
// 	protoc        v3.14.0
// source: pref_id_gen.proto

package prefIdGenpb

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type PaymentToProcess struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	ListaItems           []*Item `protobuf:"bytes,1,rep,name=lista_items,json=listaItems,proto3" json:"lista_items,omitempty"`
	Payer                *Payer  `protobuf:"bytes,2,opt,name=payer,proto3" json:"payer,omitempty"`
	StatementDescriptor  string  `protobuf:"bytes,4,opt,name=statement_descriptor,json=statementDescriptor,proto3" json:"statement_descriptor,omitempty"`
	AdditionalInfo       string  `protobuf:"bytes,5,opt,name=additional_info,json=additionalInfo,proto3" json:"additional_info,omitempty"`
	MethodOfDelivery     string  `protobuf:"bytes,6,opt,name=method_of_delivery,json=methodOfDelivery,proto3" json:"method_of_delivery,omitempty"`
	PaymentMethodOptions string  `protobuf:"bytes,7,opt,name=payment_method_options,json=paymentMethodOptions,proto3" json:"payment_method_options,omitempty"`
	TotalPrice           float32 `protobuf:"fixed32,8,opt,name=total_price,json=totalPrice,proto3" json:"total_price,omitempty"`
	HeadquarterId        int32   `protobuf:"varint,9,opt,name=headquarter_id,json=headquarterId,proto3" json:"headquarter_id,omitempty"`
	BranchId             int32   `protobuf:"varint,10,opt,name=branch_id,json=branchId,proto3" json:"branch_id,omitempty"`
}

func (x *PaymentToProcess) Reset() {
	*x = PaymentToProcess{}
	if protoimpl.UnsafeEnabled {
		mi := &file_pref_id_gen_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *PaymentToProcess) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*PaymentToProcess) ProtoMessage() {}

func (x *PaymentToProcess) ProtoReflect() protoreflect.Message {
	mi := &file_pref_id_gen_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use PaymentToProcess.ProtoReflect.Descriptor instead.
func (*PaymentToProcess) Descriptor() ([]byte, []int) {
	return file_pref_id_gen_proto_rawDescGZIP(), []int{0}
}

func (x *PaymentToProcess) GetListaItems() []*Item {
	if x != nil {
		return x.ListaItems
	}
	return nil
}

func (x *PaymentToProcess) GetPayer() *Payer {
	if x != nil {
		return x.Payer
	}
	return nil
}

func (x *PaymentToProcess) GetStatementDescriptor() string {
	if x != nil {
		return x.StatementDescriptor
	}
	return ""
}

func (x *PaymentToProcess) GetAdditionalInfo() string {
	if x != nil {
		return x.AdditionalInfo
	}
	return ""
}

func (x *PaymentToProcess) GetMethodOfDelivery() string {
	if x != nil {
		return x.MethodOfDelivery
	}
	return ""
}

func (x *PaymentToProcess) GetPaymentMethodOptions() string {
	if x != nil {
		return x.PaymentMethodOptions
	}
	return ""
}

func (x *PaymentToProcess) GetTotalPrice() float32 {
	if x != nil {
		return x.TotalPrice
	}
	return 0
}

func (x *PaymentToProcess) GetHeadquarterId() int32 {
	if x != nil {
		return x.HeadquarterId
	}
	return 0
}

func (x *PaymentToProcess) GetBranchId() int32 {
	if x != nil {
		return x.BranchId
	}
	return 0
}

type Item struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Title       string  `protobuf:"bytes,1,opt,name=title,proto3" json:"title,omitempty"`
	Description string  `protobuf:"bytes,2,opt,name=description,proto3" json:"description,omitempty"`
	Quantity    uint32  `protobuf:"varint,3,opt,name=quantity,proto3" json:"quantity,omitempty"`
	CurrencyId  string  `protobuf:"bytes,4,opt,name=currency_id,json=currencyId,proto3" json:"currency_id,omitempty"`
	UnitPrice   float32 `protobuf:"fixed32,5,opt,name=unit_price,json=unitPrice,proto3" json:"unit_price,omitempty"`
	ItemId      uint32  `protobuf:"varint,6,opt,name=item_id,json=itemId,proto3" json:"item_id,omitempty"`
	CategoryId  uint32  `protobuf:"varint,7,opt,name=category_id,json=categoryId,proto3" json:"category_id,omitempty"`
}

func (x *Item) Reset() {
	*x = Item{}
	if protoimpl.UnsafeEnabled {
		mi := &file_pref_id_gen_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Item) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Item) ProtoMessage() {}

func (x *Item) ProtoReflect() protoreflect.Message {
	mi := &file_pref_id_gen_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Item.ProtoReflect.Descriptor instead.
func (*Item) Descriptor() ([]byte, []int) {
	return file_pref_id_gen_proto_rawDescGZIP(), []int{1}
}

func (x *Item) GetTitle() string {
	if x != nil {
		return x.Title
	}
	return ""
}

func (x *Item) GetDescription() string {
	if x != nil {
		return x.Description
	}
	return ""
}

func (x *Item) GetQuantity() uint32 {
	if x != nil {
		return x.Quantity
	}
	return 0
}

func (x *Item) GetCurrencyId() string {
	if x != nil {
		return x.CurrencyId
	}
	return ""
}

func (x *Item) GetUnitPrice() float32 {
	if x != nil {
		return x.UnitPrice
	}
	return 0
}

func (x *Item) GetItemId() uint32 {
	if x != nil {
		return x.ItemId
	}
	return 0
}

func (x *Item) GetCategoryId() uint32 {
	if x != nil {
		return x.CategoryId
	}
	return 0
}

type Payer struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Email   string   `protobuf:"bytes,1,opt,name=email,proto3" json:"email,omitempty"`
	Name    string   `protobuf:"bytes,2,opt,name=name,proto3" json:"name,omitempty"`
	Surname string   `protobuf:"bytes,3,opt,name=surname,proto3" json:"surname,omitempty"`
	Phone   *Phone   `protobuf:"bytes,4,opt,name=phone,proto3" json:"phone,omitempty"`
	Address *Address `protobuf:"bytes,5,opt,name=address,proto3" json:"address,omitempty"`
}

func (x *Payer) Reset() {
	*x = Payer{}
	if protoimpl.UnsafeEnabled {
		mi := &file_pref_id_gen_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Payer) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Payer) ProtoMessage() {}

func (x *Payer) ProtoReflect() protoreflect.Message {
	mi := &file_pref_id_gen_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Payer.ProtoReflect.Descriptor instead.
func (*Payer) Descriptor() ([]byte, []int) {
	return file_pref_id_gen_proto_rawDescGZIP(), []int{2}
}

func (x *Payer) GetEmail() string {
	if x != nil {
		return x.Email
	}
	return ""
}

func (x *Payer) GetName() string {
	if x != nil {
		return x.Name
	}
	return ""
}

func (x *Payer) GetSurname() string {
	if x != nil {
		return x.Surname
	}
	return ""
}

func (x *Payer) GetPhone() *Phone {
	if x != nil {
		return x.Phone
	}
	return nil
}

func (x *Payer) GetAddress() *Address {
	if x != nil {
		return x.Address
	}
	return nil
}

type Phone struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	AreaCode string `protobuf:"bytes,1,opt,name=area_code,json=areaCode,proto3" json:"area_code,omitempty"`
	Number   string `protobuf:"bytes,2,opt,name=number,proto3" json:"number,omitempty"`
}

func (x *Phone) Reset() {
	*x = Phone{}
	if protoimpl.UnsafeEnabled {
		mi := &file_pref_id_gen_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Phone) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Phone) ProtoMessage() {}

func (x *Phone) ProtoReflect() protoreflect.Message {
	mi := &file_pref_id_gen_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Phone.ProtoReflect.Descriptor instead.
func (*Phone) Descriptor() ([]byte, []int) {
	return file_pref_id_gen_proto_rawDescGZIP(), []int{3}
}

func (x *Phone) GetAreaCode() string {
	if x != nil {
		return x.AreaCode
	}
	return ""
}

func (x *Phone) GetNumber() string {
	if x != nil {
		return x.Number
	}
	return ""
}

type Address struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	ZipCode      string `protobuf:"bytes,1,opt,name=zip_code,json=zipCode,proto3" json:"zip_code,omitempty"`
	StreetName   string `protobuf:"bytes,2,opt,name=street_name,json=streetName,proto3" json:"street_name,omitempty"`
	StreetNumber uint32 `protobuf:"varint,3,opt,name=street_number,json=streetNumber,proto3" json:"street_number,omitempty"`
}

func (x *Address) Reset() {
	*x = Address{}
	if protoimpl.UnsafeEnabled {
		mi := &file_pref_id_gen_proto_msgTypes[4]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Address) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Address) ProtoMessage() {}

func (x *Address) ProtoReflect() protoreflect.Message {
	mi := &file_pref_id_gen_proto_msgTypes[4]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Address.ProtoReflect.Descriptor instead.
func (*Address) Descriptor() ([]byte, []int) {
	return file_pref_id_gen_proto_rawDescGZIP(), []int{4}
}

func (x *Address) GetZipCode() string {
	if x != nil {
		return x.ZipCode
	}
	return ""
}

func (x *Address) GetStreetName() string {
	if x != nil {
		return x.StreetName
	}
	return ""
}

func (x *Address) GetStreetNumber() uint32 {
	if x != nil {
		return x.StreetNumber
	}
	return 0
}

type ItemStatusResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	StatusSuccessful bool   `protobuf:"varint,1,opt,name=status_successful,json=statusSuccessful,proto3" json:"status_successful,omitempty"`
	Error            string `protobuf:"bytes,2,opt,name=error,proto3" json:"error,omitempty"`
	OrderCode        int64  `protobuf:"varint,3,opt,name=order_code,json=orderCode,proto3" json:"order_code,omitempty"`
	PrefIdGen        string `protobuf:"bytes,4,opt,name=pref_id_gen,json=prefIdGen,proto3" json:"pref_id_gen,omitempty"`
}

func (x *ItemStatusResponse) Reset() {
	*x = ItemStatusResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_pref_id_gen_proto_msgTypes[5]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *ItemStatusResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ItemStatusResponse) ProtoMessage() {}

func (x *ItemStatusResponse) ProtoReflect() protoreflect.Message {
	mi := &file_pref_id_gen_proto_msgTypes[5]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use ItemStatusResponse.ProtoReflect.Descriptor instead.
func (*ItemStatusResponse) Descriptor() ([]byte, []int) {
	return file_pref_id_gen_proto_rawDescGZIP(), []int{5}
}

func (x *ItemStatusResponse) GetStatusSuccessful() bool {
	if x != nil {
		return x.StatusSuccessful
	}
	return false
}

func (x *ItemStatusResponse) GetError() string {
	if x != nil {
		return x.Error
	}
	return ""
}

func (x *ItemStatusResponse) GetOrderCode() int64 {
	if x != nil {
		return x.OrderCode
	}
	return 0
}

func (x *ItemStatusResponse) GetPrefIdGen() string {
	if x != nil {
		return x.PrefIdGen
	}
	return ""
}

var File_pref_id_gen_proto protoreflect.FileDescriptor

var file_pref_id_gen_proto_rawDesc = []byte{
	0x0a, 0x11, 0x70, 0x72, 0x65, 0x66, 0x5f, 0x69, 0x64, 0x5f, 0x67, 0x65, 0x6e, 0x2e, 0x70, 0x72,
	0x6f, 0x74, 0x6f, 0x12, 0x18, 0x70, 0x72, 0x65, 0x66, 0x5f, 0x69, 0x64, 0x5f, 0x67, 0x65, 0x6e,
	0x5f, 0x70, 0x62, 0x2e, 0x70, 0x72, 0x65, 0x66, 0x49, 0x64, 0x47, 0x65, 0x6e, 0x22, 0xc4, 0x03,
	0x0a, 0x10, 0x50, 0x61, 0x79, 0x6d, 0x65, 0x6e, 0x74, 0x54, 0x6f, 0x50, 0x72, 0x6f, 0x63, 0x65,
	0x73, 0x73, 0x12, 0x3f, 0x0a, 0x0b, 0x6c, 0x69, 0x73, 0x74, 0x61, 0x5f, 0x69, 0x74, 0x65, 0x6d,
	0x73, 0x18, 0x01, 0x20, 0x03, 0x28, 0x0b, 0x32, 0x1e, 0x2e, 0x70, 0x72, 0x65, 0x66, 0x5f, 0x69,
	0x64, 0x5f, 0x67, 0x65, 0x6e, 0x5f, 0x70, 0x62, 0x2e, 0x70, 0x72, 0x65, 0x66, 0x49, 0x64, 0x47,
	0x65, 0x6e, 0x2e, 0x49, 0x74, 0x65, 0x6d, 0x52, 0x0a, 0x6c, 0x69, 0x73, 0x74, 0x61, 0x49, 0x74,
	0x65, 0x6d, 0x73, 0x12, 0x35, 0x0a, 0x05, 0x70, 0x61, 0x79, 0x65, 0x72, 0x18, 0x02, 0x20, 0x01,
	0x28, 0x0b, 0x32, 0x1f, 0x2e, 0x70, 0x72, 0x65, 0x66, 0x5f, 0x69, 0x64, 0x5f, 0x67, 0x65, 0x6e,
	0x5f, 0x70, 0x62, 0x2e, 0x70, 0x72, 0x65, 0x66, 0x49, 0x64, 0x47, 0x65, 0x6e, 0x2e, 0x50, 0x61,
	0x79, 0x65, 0x72, 0x52, 0x05, 0x70, 0x61, 0x79, 0x65, 0x72, 0x12, 0x31, 0x0a, 0x14, 0x73, 0x74,
	0x61, 0x74, 0x65, 0x6d, 0x65, 0x6e, 0x74, 0x5f, 0x64, 0x65, 0x73, 0x63, 0x72, 0x69, 0x70, 0x74,
	0x6f, 0x72, 0x18, 0x04, 0x20, 0x01, 0x28, 0x09, 0x52, 0x13, 0x73, 0x74, 0x61, 0x74, 0x65, 0x6d,
	0x65, 0x6e, 0x74, 0x44, 0x65, 0x73, 0x63, 0x72, 0x69, 0x70, 0x74, 0x6f, 0x72, 0x12, 0x27, 0x0a,
	0x0f, 0x61, 0x64, 0x64, 0x69, 0x74, 0x69, 0x6f, 0x6e, 0x61, 0x6c, 0x5f, 0x69, 0x6e, 0x66, 0x6f,
	0x18, 0x05, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0e, 0x61, 0x64, 0x64, 0x69, 0x74, 0x69, 0x6f, 0x6e,
	0x61, 0x6c, 0x49, 0x6e, 0x66, 0x6f, 0x12, 0x2c, 0x0a, 0x12, 0x6d, 0x65, 0x74, 0x68, 0x6f, 0x64,
	0x5f, 0x6f, 0x66, 0x5f, 0x64, 0x65, 0x6c, 0x69, 0x76, 0x65, 0x72, 0x79, 0x18, 0x06, 0x20, 0x01,
	0x28, 0x09, 0x52, 0x10, 0x6d, 0x65, 0x74, 0x68, 0x6f, 0x64, 0x4f, 0x66, 0x44, 0x65, 0x6c, 0x69,
	0x76, 0x65, 0x72, 0x79, 0x12, 0x34, 0x0a, 0x16, 0x70, 0x61, 0x79, 0x6d, 0x65, 0x6e, 0x74, 0x5f,
	0x6d, 0x65, 0x74, 0x68, 0x6f, 0x64, 0x5f, 0x6f, 0x70, 0x74, 0x69, 0x6f, 0x6e, 0x73, 0x18, 0x07,
	0x20, 0x01, 0x28, 0x09, 0x52, 0x14, 0x70, 0x61, 0x79, 0x6d, 0x65, 0x6e, 0x74, 0x4d, 0x65, 0x74,
	0x68, 0x6f, 0x64, 0x4f, 0x70, 0x74, 0x69, 0x6f, 0x6e, 0x73, 0x12, 0x1f, 0x0a, 0x0b, 0x74, 0x6f,
	0x74, 0x61, 0x6c, 0x5f, 0x70, 0x72, 0x69, 0x63, 0x65, 0x18, 0x08, 0x20, 0x01, 0x28, 0x02, 0x52,
	0x0a, 0x74, 0x6f, 0x74, 0x61, 0x6c, 0x50, 0x72, 0x69, 0x63, 0x65, 0x12, 0x25, 0x0a, 0x0e, 0x68,
	0x65, 0x61, 0x64, 0x71, 0x75, 0x61, 0x72, 0x74, 0x65, 0x72, 0x5f, 0x69, 0x64, 0x18, 0x09, 0x20,
	0x01, 0x28, 0x05, 0x52, 0x0d, 0x68, 0x65, 0x61, 0x64, 0x71, 0x75, 0x61, 0x72, 0x74, 0x65, 0x72,
	0x49, 0x64, 0x12, 0x1b, 0x0a, 0x09, 0x62, 0x72, 0x61, 0x6e, 0x63, 0x68, 0x5f, 0x69, 0x64, 0x18,
	0x0a, 0x20, 0x01, 0x28, 0x05, 0x52, 0x08, 0x62, 0x72, 0x61, 0x6e, 0x63, 0x68, 0x49, 0x64, 0x4a,
	0x04, 0x08, 0x03, 0x10, 0x04, 0x52, 0x0d, 0x70, 0x72, 0x65, 0x66, 0x65, 0x72, 0x65, 0x6e, 0x63,
	0x65, 0x5f, 0x69, 0x64, 0x22, 0xd4, 0x01, 0x0a, 0x04, 0x49, 0x74, 0x65, 0x6d, 0x12, 0x14, 0x0a,
	0x05, 0x74, 0x69, 0x74, 0x6c, 0x65, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x05, 0x74, 0x69,
	0x74, 0x6c, 0x65, 0x12, 0x20, 0x0a, 0x0b, 0x64, 0x65, 0x73, 0x63, 0x72, 0x69, 0x70, 0x74, 0x69,
	0x6f, 0x6e, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0b, 0x64, 0x65, 0x73, 0x63, 0x72, 0x69,
	0x70, 0x74, 0x69, 0x6f, 0x6e, 0x12, 0x1a, 0x0a, 0x08, 0x71, 0x75, 0x61, 0x6e, 0x74, 0x69, 0x74,
	0x79, 0x18, 0x03, 0x20, 0x01, 0x28, 0x0d, 0x52, 0x08, 0x71, 0x75, 0x61, 0x6e, 0x74, 0x69, 0x74,
	0x79, 0x12, 0x1f, 0x0a, 0x0b, 0x63, 0x75, 0x72, 0x72, 0x65, 0x6e, 0x63, 0x79, 0x5f, 0x69, 0x64,
	0x18, 0x04, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0a, 0x63, 0x75, 0x72, 0x72, 0x65, 0x6e, 0x63, 0x79,
	0x49, 0x64, 0x12, 0x1d, 0x0a, 0x0a, 0x75, 0x6e, 0x69, 0x74, 0x5f, 0x70, 0x72, 0x69, 0x63, 0x65,
	0x18, 0x05, 0x20, 0x01, 0x28, 0x02, 0x52, 0x09, 0x75, 0x6e, 0x69, 0x74, 0x50, 0x72, 0x69, 0x63,
	0x65, 0x12, 0x17, 0x0a, 0x07, 0x69, 0x74, 0x65, 0x6d, 0x5f, 0x69, 0x64, 0x18, 0x06, 0x20, 0x01,
	0x28, 0x0d, 0x52, 0x06, 0x69, 0x74, 0x65, 0x6d, 0x49, 0x64, 0x12, 0x1f, 0x0a, 0x0b, 0x63, 0x61,
	0x74, 0x65, 0x67, 0x6f, 0x72, 0x79, 0x5f, 0x69, 0x64, 0x18, 0x07, 0x20, 0x01, 0x28, 0x0d, 0x52,
	0x0a, 0x63, 0x61, 0x74, 0x65, 0x67, 0x6f, 0x72, 0x79, 0x49, 0x64, 0x22, 0xbf, 0x01, 0x0a, 0x05,
	0x50, 0x61, 0x79, 0x65, 0x72, 0x12, 0x14, 0x0a, 0x05, 0x65, 0x6d, 0x61, 0x69, 0x6c, 0x18, 0x01,
	0x20, 0x01, 0x28, 0x09, 0x52, 0x05, 0x65, 0x6d, 0x61, 0x69, 0x6c, 0x12, 0x12, 0x0a, 0x04, 0x6e,
	0x61, 0x6d, 0x65, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x6e, 0x61, 0x6d, 0x65, 0x12,
	0x18, 0x0a, 0x07, 0x73, 0x75, 0x72, 0x6e, 0x61, 0x6d, 0x65, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09,
	0x52, 0x07, 0x73, 0x75, 0x72, 0x6e, 0x61, 0x6d, 0x65, 0x12, 0x35, 0x0a, 0x05, 0x70, 0x68, 0x6f,
	0x6e, 0x65, 0x18, 0x04, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x1f, 0x2e, 0x70, 0x72, 0x65, 0x66, 0x5f,
	0x69, 0x64, 0x5f, 0x67, 0x65, 0x6e, 0x5f, 0x70, 0x62, 0x2e, 0x70, 0x72, 0x65, 0x66, 0x49, 0x64,
	0x47, 0x65, 0x6e, 0x2e, 0x50, 0x68, 0x6f, 0x6e, 0x65, 0x52, 0x05, 0x70, 0x68, 0x6f, 0x6e, 0x65,
	0x12, 0x3b, 0x0a, 0x07, 0x61, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x18, 0x05, 0x20, 0x01, 0x28,
	0x0b, 0x32, 0x21, 0x2e, 0x70, 0x72, 0x65, 0x66, 0x5f, 0x69, 0x64, 0x5f, 0x67, 0x65, 0x6e, 0x5f,
	0x70, 0x62, 0x2e, 0x70, 0x72, 0x65, 0x66, 0x49, 0x64, 0x47, 0x65, 0x6e, 0x2e, 0x41, 0x64, 0x64,
	0x72, 0x65, 0x73, 0x73, 0x52, 0x07, 0x61, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x22, 0x3c, 0x0a,
	0x05, 0x50, 0x68, 0x6f, 0x6e, 0x65, 0x12, 0x1b, 0x0a, 0x09, 0x61, 0x72, 0x65, 0x61, 0x5f, 0x63,
	0x6f, 0x64, 0x65, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x08, 0x61, 0x72, 0x65, 0x61, 0x43,
	0x6f, 0x64, 0x65, 0x12, 0x16, 0x0a, 0x06, 0x6e, 0x75, 0x6d, 0x62, 0x65, 0x72, 0x18, 0x02, 0x20,
	0x01, 0x28, 0x09, 0x52, 0x06, 0x6e, 0x75, 0x6d, 0x62, 0x65, 0x72, 0x22, 0x6a, 0x0a, 0x07, 0x41,
	0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x12, 0x19, 0x0a, 0x08, 0x7a, 0x69, 0x70, 0x5f, 0x63, 0x6f,
	0x64, 0x65, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x7a, 0x69, 0x70, 0x43, 0x6f, 0x64,
	0x65, 0x12, 0x1f, 0x0a, 0x0b, 0x73, 0x74, 0x72, 0x65, 0x65, 0x74, 0x5f, 0x6e, 0x61, 0x6d, 0x65,
	0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0a, 0x73, 0x74, 0x72, 0x65, 0x65, 0x74, 0x4e, 0x61,
	0x6d, 0x65, 0x12, 0x23, 0x0a, 0x0d, 0x73, 0x74, 0x72, 0x65, 0x65, 0x74, 0x5f, 0x6e, 0x75, 0x6d,
	0x62, 0x65, 0x72, 0x18, 0x03, 0x20, 0x01, 0x28, 0x0d, 0x52, 0x0c, 0x73, 0x74, 0x72, 0x65, 0x65,
	0x74, 0x4e, 0x75, 0x6d, 0x62, 0x65, 0x72, 0x22, 0x96, 0x01, 0x0a, 0x12, 0x49, 0x74, 0x65, 0x6d,
	0x53, 0x74, 0x61, 0x74, 0x75, 0x73, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x2b,
	0x0a, 0x11, 0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x5f, 0x73, 0x75, 0x63, 0x63, 0x65, 0x73, 0x73,
	0x66, 0x75, 0x6c, 0x18, 0x01, 0x20, 0x01, 0x28, 0x08, 0x52, 0x10, 0x73, 0x74, 0x61, 0x74, 0x75,
	0x73, 0x53, 0x75, 0x63, 0x63, 0x65, 0x73, 0x73, 0x66, 0x75, 0x6c, 0x12, 0x14, 0x0a, 0x05, 0x65,
	0x72, 0x72, 0x6f, 0x72, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x05, 0x65, 0x72, 0x72, 0x6f,
	0x72, 0x12, 0x1d, 0x0a, 0x0a, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x5f, 0x63, 0x6f, 0x64, 0x65, 0x18,
	0x03, 0x20, 0x01, 0x28, 0x03, 0x52, 0x09, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x43, 0x6f, 0x64, 0x65,
	0x12, 0x1e, 0x0a, 0x0b, 0x70, 0x72, 0x65, 0x66, 0x5f, 0x69, 0x64, 0x5f, 0x67, 0x65, 0x6e, 0x18,
	0x04, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x70, 0x72, 0x65, 0x66, 0x49, 0x64, 0x47, 0x65, 0x6e,
	0x32, 0x92, 0x01, 0x0a, 0x0f, 0x50, 0x61, 0x79, 0x6d, 0x65, 0x6e, 0x74, 0x50, 0x72, 0x6f, 0x64,
	0x75, 0x63, 0x74, 0x73, 0x12, 0x7f, 0x0a, 0x23, 0x50, 0x72, 0x65, 0x66, 0x49, 0x64, 0x47, 0x65,
	0x6e, 0x41, 0x6e, 0x64, 0x49, 0x6e, 0x73, 0x65, 0x72, 0x74, 0x50, 0x72, 0x6f, 0x64, 0x75, 0x63,
	0x74, 0x73, 0x50, 0x75, 0x72, 0x63, 0x68, 0x61, 0x73, 0x65, 0x64, 0x12, 0x2a, 0x2e, 0x70, 0x72,
	0x65, 0x66, 0x5f, 0x69, 0x64, 0x5f, 0x67, 0x65, 0x6e, 0x5f, 0x70, 0x62, 0x2e, 0x70, 0x72, 0x65,
	0x66, 0x49, 0x64, 0x47, 0x65, 0x6e, 0x2e, 0x50, 0x61, 0x79, 0x6d, 0x65, 0x6e, 0x74, 0x54, 0x6f,
	0x50, 0x72, 0x6f, 0x63, 0x65, 0x73, 0x73, 0x1a, 0x2c, 0x2e, 0x70, 0x72, 0x65, 0x66, 0x5f, 0x69,
	0x64, 0x5f, 0x67, 0x65, 0x6e, 0x5f, 0x70, 0x62, 0x2e, 0x70, 0x72, 0x65, 0x66, 0x49, 0x64, 0x47,
	0x65, 0x6e, 0x2e, 0x49, 0x74, 0x65, 0x6d, 0x53, 0x74, 0x61, 0x74, 0x75, 0x73, 0x52, 0x65, 0x73,
	0x70, 0x6f, 0x6e, 0x73, 0x65, 0x42, 0x19, 0x5a, 0x17, 0x70, 0x72, 0x65, 0x66, 0x49, 0x64, 0x47,
	0x65, 0x6e, 0x70, 0x62, 0x3b, 0x70, 0x72, 0x65, 0x66, 0x49, 0x64, 0x47, 0x65, 0x6e, 0x70, 0x62,
	0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_pref_id_gen_proto_rawDescOnce sync.Once
	file_pref_id_gen_proto_rawDescData = file_pref_id_gen_proto_rawDesc
)

func file_pref_id_gen_proto_rawDescGZIP() []byte {
	file_pref_id_gen_proto_rawDescOnce.Do(func() {
		file_pref_id_gen_proto_rawDescData = protoimpl.X.CompressGZIP(file_pref_id_gen_proto_rawDescData)
	})
	return file_pref_id_gen_proto_rawDescData
}

var file_pref_id_gen_proto_msgTypes = make([]protoimpl.MessageInfo, 6)
var file_pref_id_gen_proto_goTypes = []interface{}{
	(*PaymentToProcess)(nil),   // 0: pref_id_gen_pb.prefIdGen.PaymentToProcess
	(*Item)(nil),               // 1: pref_id_gen_pb.prefIdGen.Item
	(*Payer)(nil),              // 2: pref_id_gen_pb.prefIdGen.Payer
	(*Phone)(nil),              // 3: pref_id_gen_pb.prefIdGen.Phone
	(*Address)(nil),            // 4: pref_id_gen_pb.prefIdGen.Address
	(*ItemStatusResponse)(nil), // 5: pref_id_gen_pb.prefIdGen.ItemStatusResponse
}
var file_pref_id_gen_proto_depIdxs = []int32{
	1, // 0: pref_id_gen_pb.prefIdGen.PaymentToProcess.lista_items:type_name -> pref_id_gen_pb.prefIdGen.Item
	2, // 1: pref_id_gen_pb.prefIdGen.PaymentToProcess.payer:type_name -> pref_id_gen_pb.prefIdGen.Payer
	3, // 2: pref_id_gen_pb.prefIdGen.Payer.phone:type_name -> pref_id_gen_pb.prefIdGen.Phone
	4, // 3: pref_id_gen_pb.prefIdGen.Payer.address:type_name -> pref_id_gen_pb.prefIdGen.Address
	0, // 4: pref_id_gen_pb.prefIdGen.PaymentProducts.PrefIdGenAndInsertProductsPurchased:input_type -> pref_id_gen_pb.prefIdGen.PaymentToProcess
	5, // 5: pref_id_gen_pb.prefIdGen.PaymentProducts.PrefIdGenAndInsertProductsPurchased:output_type -> pref_id_gen_pb.prefIdGen.ItemStatusResponse
	5, // [5:6] is the sub-list for method output_type
	4, // [4:5] is the sub-list for method input_type
	4, // [4:4] is the sub-list for extension type_name
	4, // [4:4] is the sub-list for extension extendee
	0, // [0:4] is the sub-list for field type_name
}

func init() { file_pref_id_gen_proto_init() }
func file_pref_id_gen_proto_init() {
	if File_pref_id_gen_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_pref_id_gen_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*PaymentToProcess); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_pref_id_gen_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Item); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_pref_id_gen_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Payer); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_pref_id_gen_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Phone); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_pref_id_gen_proto_msgTypes[4].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Address); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_pref_id_gen_proto_msgTypes[5].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*ItemStatusResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_pref_id_gen_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   6,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_pref_id_gen_proto_goTypes,
		DependencyIndexes: file_pref_id_gen_proto_depIdxs,
		MessageInfos:      file_pref_id_gen_proto_msgTypes,
	}.Build()
	File_pref_id_gen_proto = out.File
	file_pref_id_gen_proto_rawDesc = nil
	file_pref_id_gen_proto_goTypes = nil
	file_pref_id_gen_proto_depIdxs = nil
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConnInterface

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion6

// PaymentProductsClient is the client API for PaymentProducts service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://godoc.org/google.golang.org/grpc#ClientConn.NewStream.
type PaymentProductsClient interface {
	PrefIdGenAndInsertProductsPurchased(ctx context.Context, in *PaymentToProcess, opts ...grpc.CallOption) (*ItemStatusResponse, error)
}

type paymentProductsClient struct {
	cc grpc.ClientConnInterface
}

func NewPaymentProductsClient(cc grpc.ClientConnInterface) PaymentProductsClient {
	return &paymentProductsClient{cc}
}

func (c *paymentProductsClient) PrefIdGenAndInsertProductsPurchased(ctx context.Context, in *PaymentToProcess, opts ...grpc.CallOption) (*ItemStatusResponse, error) {
	out := new(ItemStatusResponse)
	err := c.cc.Invoke(ctx, "/pref_id_gen_pb.prefIdGen.PaymentProducts/PrefIdGenAndInsertProductsPurchased", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// PaymentProductsServer is the server API for PaymentProducts service.
type PaymentProductsServer interface {
	PrefIdGenAndInsertProductsPurchased(context.Context, *PaymentToProcess) (*ItemStatusResponse, error)
}

// UnimplementedPaymentProductsServer can be embedded to have forward compatible implementations.
type UnimplementedPaymentProductsServer struct {
}

func (*UnimplementedPaymentProductsServer) PrefIdGenAndInsertProductsPurchased(context.Context, *PaymentToProcess) (*ItemStatusResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method PrefIdGenAndInsertProductsPurchased not implemented")
}

func RegisterPaymentProductsServer(s *grpc.Server, srv PaymentProductsServer) {
	s.RegisterService(&_PaymentProducts_serviceDesc, srv)
}

func _PaymentProducts_PrefIdGenAndInsertProductsPurchased_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(PaymentToProcess)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(PaymentProductsServer).PrefIdGenAndInsertProductsPurchased(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/pref_id_gen_pb.prefIdGen.PaymentProducts/PrefIdGenAndInsertProductsPurchased",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(PaymentProductsServer).PrefIdGenAndInsertProductsPurchased(ctx, req.(*PaymentToProcess))
	}
	return interceptor(ctx, in, info, handler)
}

var _PaymentProducts_serviceDesc = grpc.ServiceDesc{
	ServiceName: "pref_id_gen_pb.prefIdGen.PaymentProducts",
	HandlerType: (*PaymentProductsServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "PrefIdGenAndInsertProductsPurchased",
			Handler:    _PaymentProducts_PrefIdGenAndInsertProductsPurchased_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "pref_id_gen.proto",
}
