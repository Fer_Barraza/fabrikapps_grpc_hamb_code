package handler

import (
	"context"
	"log"
	"time"

	"google.golang.org/grpc"

	product_model "fabrikapps/pref_id_gen/pkg/domain/product"
	"fabrikapps/pref_id_gen/pkg/infrastrucure/delivery/grpc/prefIdGenpb"
	app_get_service "fabrikapps/pref_id_gen/pkg/service"
)

type service struct {
	serv app_get_service.Service
}

// NewProductListServerGrpc ...
func NewInsertProductsServerGrpc(gserver *grpc.Server, insertserv app_get_service.Service) {
	tempService := &service{
		serv: insertserv,
	}
	prefIdGenpb.RegisterPaymentProductsServer(gserver, tempService)
}

func (s *service) PrefIdGenAndInsertProductsPurchased(ctx context.Context, reqPaymentToIns *prefIdGenpb.PaymentToProcess) (*prefIdGenpb.ItemStatusResponse, error) {
	ctx, cancelfunc := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancelfunc()
	//headquarterId := reqPaymentToIns.HeadquarterId
	branchId := reqPaymentToIns.BranchId
	var modelListItems []product_model.Item
	for _, item := range reqPaymentToIns.ListaItems {
		tempModelItem := &product_model.Item{
			Title:       &item.Title,
			Description: &item.Description,
			Quantity:    &item.Quantity,
			UnitPrice:   &item.UnitPrice,
		}
		modelListItems = append(modelListItems, *tempModelItem)
	}
	phoneToIns := &product_model.Phone{
		AreaCode: &reqPaymentToIns.Payer.Phone.AreaCode,
		Number:   &reqPaymentToIns.Payer.Phone.Number,
	}
	streetToIns := &product_model.Address{
		StreetName:   &reqPaymentToIns.Payer.Address.StreetName,
		StreetNumber: &reqPaymentToIns.Payer.Address.StreetNumber,
	}
	payerToIns := &product_model.Payer{
		Email:   &reqPaymentToIns.Payer.Email,
		Name:    &reqPaymentToIns.Payer.Name,
		Surname: &reqPaymentToIns.Payer.Surname,
		Phone:   phoneToIns,
		Address: streetToIns,
	}
	temp := "atm"
	temp2 := "ticket"
	excludedPaymentType := []product_model.ExcludedPaymentType{
		product_model.ExcludedPaymentType{
			ID: &temp,
		},
		product_model.ExcludedPaymentType{
			ID: &temp2,
		},
	}
	paymentMethodsToExcl := &product_model.PaymentMethods{
		Installments:         1,
		ExcludedPaymentTypes: excludedPaymentType,
	}
	paymentToInsert := &product_model.CartInfoPrefID{
		Items:               modelListItems,
		Payer:               payerToIns,
		StatementDescriptor: &reqPaymentToIns.StatementDescriptor,
		AdditionalInfo:      &reqPaymentToIns.AdditionalInfo,
		PaymentMethods:      paymentMethodsToExcl,
	}

	bytes, err := paymentToInsert.Marshal()

	prefIdGen, err := s.serv.PrefIdGenService(bytes, branchId)
	if err != nil {
		log.Printf("Error %s when calling the setProduct\n", err)
		return nil, err
	}
	// print(prefIdGen)
	// print(err.Error())

	response := &prefIdGenpb.ItemStatusResponse{
		StatusSuccessful: true,
		Error:            "No errors",
		OrderCode:        0,
		PrefIdGen:        prefIdGen,
	}

	return response, nil
}
