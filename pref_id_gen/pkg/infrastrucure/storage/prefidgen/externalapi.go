package prefidgen

import (
	"bytes"
	"encoding/json"
	"fabrikapps/pref_id_gen/pkg/domain/product"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	//product_model "fabrikapps/pref_id_gen/pkg/domain/product"
)

// Storage keeps data in db
type mpApi struct {
}

func NewMPConnection() product.Repository {
	return &mpApi{}
}

func (_ *mpApi) InsertPaymentRepo(jsonString []byte) (string, error) {
	//ctx := context.Background()
	accessToken := os.Getenv("ACCESSTOKENMP")
	var bearer = "Bearer " + accessToken
	//url := "https://api.mercadopago.com/checkout/preferences?access_token=" + accessToken
	url := "https://api.mercadopago.com/checkout/preferences"
	fmt.Println("URL:>", url)
	//var jsonStr = []byte(`{"title":"Buy cheese and bread for breakfast."}`)
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonString))
	req.Header.Add("Authorization", bearer)
	//req.Header.Set("X-Custom-Header", "myvalue")
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		//panic(err)
		return "", err
	}
	defer resp.Body.Close()

	//fmt.Println("response Status:", resp.Status)
	//fmt.Println("response Headers:", resp.Header)
	body, _ := ioutil.ReadAll(resp.Body)
	// fmt.Println("response Body:", string(body))
	var result map[string]interface{}
	json.Unmarshal([]byte(body), &result)
	//var prefIdGen string
	prefIdGen := result["id"].(string)
	// print(prefIdGen)

	// print(cartInfoPrefID.AdditionalInfo)
	//print(result)
	return prefIdGen, err
}
func (_ *mpApi) PrefIdGenRepo(jsonString []byte, accessToken string) (string, error) {
	//ctx := context.Background()
	//accessToken := "TEST-4561833546465531-111023-0fec7fec733cdb0067ed1ab1ec9209b1-236976484"
	var bearer = "Bearer " + accessToken
	//url := "https://api.mercadopago.com/checkout/preferences?access_token=" + accessToken
	url := "https://api.mercadopago.com/checkout/preferences"
	fmt.Println("URL:>", url)
	//var jsonStr = []byte(`{"title":"Buy cheese and bread for breakfast."}`)
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonString))
	req.Header.Add("Authorization", bearer)
	//req.Header.Set("X-Custom-Header", "myvalue")
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		//panic(err)
		return "", err
	}
	defer resp.Body.Close()

	//fmt.Println("response Status:", resp.Status)
	//fmt.Println("response Headers:", resp.Header)
	body, err := ioutil.ReadAll(resp.Body)
	// fmt.Println("response Body:", string(body))
	var result map[string]interface{}
	json.Unmarshal([]byte(body), &result)
	//var prefIdGen string
	prefIdGen := result["id"].(string)
	// print(prefIdGen)

	// print(cartInfoPrefID.AdditionalInfo)
	//print(result)
	return prefIdGen, err
}
