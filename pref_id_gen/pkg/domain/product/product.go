package product

// type PaymentToProcess struct {
// 	ListaItems           []*Item
// 	Payer                *Payer
// 	StatementDescriptor  string
// 	AdditionalInfo       string
// 	MethodOfDelivery     string
// 	PaymentMethodOptions string
// 	TotalPrice           float32
// }

// type Item struct {
// 	// Title       string
// 	// Description string
// 	// Quantity    uint32
// 	// CurrencyId  string
// 	// UnitPrice   float32
// 	ItemId      uint32
// 	CategoryId  uint32
// 	Title       *string  `json:"title,omitempty"`
// 	Description *string  `json:"description,omitempty"`
// 	Quantity    *int64   `json:"quantity,omitempty"`
// 	CurrencyID  *string  `json:"currency_id,omitempty"`
// 	UnitPrice   *float64 `json:"unit_price,omitempty"`
// }
// type ItemDetailResponse struct {
// 	Quantity   uint32
// 	UnitPrice  float32
// 	ItemId     uint32
// 	CategoryId uint32
// 	ItemTitle  string
// }

// // type Payer struct {
// // 	Email   string
// // 	Name    string
// // 	Surname string
// // 	Phone   *Phone
// // 	Address *Address
// // }
// // type Phone struct {
// // 	AreaCode string
// // 	Number   string
// // }
// // type Address struct {
// // 	ZipCode      string
// // 	StreetName   string
// // 	StreetNumber uint32
// // }

// type ProductList struct {
// 	ListOfItems []*Item
// }

// type OrdersRequest struct {
// 	ListaItems   []*ItemDetailResponse
// 	StreetName   string
// 	StreetNumber uint32
// 	PhoneNumber  string
// 	Email        string
// 	Name         string
// 	Surname      string
// 	OrderState   int64
// 	// AdditionalInfo       string
// 	MethodOfDelivery     string
// 	PaymentMethodOptions string
// 	TotalPrice           float32
// 	OrderDate            string
// 	OrderTime            string
// 	OrderCode            int64
// 	PayerId              int
// }

// func (o *OrdersRequest) GetListaItems() []*ItemDetailResponse {
// 	return o.ListaItems
// }
