// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse and unparse this JSON data, add this code to your project and do:
//
//    cartInfoPrefID, err := UnmarshalCartInfoPrefID(bytes)
//    bytes, err = cartInfoPrefID.Marshal()

package product

import "encoding/json"

func UnmarshalCartInfoPrefID(data []byte) (CartInfoPrefID, error) {
	var r CartInfoPrefID
	err := json.Unmarshal(data, &r)
	return r, err
}

func (r *CartInfoPrefID) Marshal() ([]byte, error) {
	return json.Marshal(r)
}

type CartInfoPrefID struct {
	Items               []Item          `json:"items,omitempty"`
	Payer               *Payer          `json:"payer,omitempty"`
	PaymentMethods      *PaymentMethods `json:"payment_methods,omitempty"`
	StatementDescriptor *string         `json:"statement_descriptor,omitempty"`
	AdditionalInfo      *string         `json:"additional_info,omitempty"`
}

type Item struct {
	Title       *string  `json:"title,omitempty"`
	Description *string  `json:"description,omitempty"`
	Quantity    *uint32  `json:"quantity,omitempty"`
	CurrencyID  *string  `json:"currency_id,omitempty"`
	UnitPrice   *float32 `json:"unit_price,omitempty"`
}

type Payer struct {
	Email   *string  `json:"email,omitempty"`
	Name    *string  `json:"name,omitempty"`
	Surname *string  `json:"surname,omitempty"`
	Phone   *Phone   `json:"phone,omitempty"`
	Address *Address `json:"address,omitempty"`
}

type Address struct {
	ZipCode      *string `json:"zip_code,omitempty"`
	StreetName   *string `json:"street_name,omitempty"`
	StreetNumber *uint32 `json:"street_number,omitempty"`
}

type Phone struct {
	AreaCode *string `json:"area_code,omitempty"`
	Number   *string `json:"number,omitempty"`
}

type PaymentMethods struct {
	ExcludedPaymentTypes []ExcludedPaymentType `json:"excluded_payment_types,omitempty"`
	Installments         uint32                `json:"installments,omitempty"`
}

type ExcludedPaymentType struct {
	ID *string `json:"id,omitempty"`
}
