package product

// Repository provides access to role repository
type Repository interface {
	InsertPaymentRepo(jsonString []byte) (string, error)
	PrefIdGenRepo(jsonString []byte, accessToken string) (string, error)
}
