package main

import (
	"fabrikapps/pref_id_gen/pkg/infrastrucure/delivery/handler"
	"fabrikapps/pref_id_gen/pkg/infrastrucure/storage/prefidgen"
	"fabrikapps/pref_id_gen/pkg/service"

	"log"
	"net"

	"google.golang.org/grpc"
)

func main() {
	log.Println("iniciando grpc server")

	// dbConn, err := database.GetConnection()
	// if err != nil {
	// 	fmt.Println(err)
	// }
	// defer dbConn.Close()

	lis, err := net.Listen("tcp", ":50054")
	if err != nil {
		log.Fatalf("Failed to listen: %v", err)
	}

	rp := prefidgen.NewMPConnection()
	ss := service.NewService(rp)

	grpc_srv := grpc.NewServer()
	handler.NewInsertProductsServerGrpc(grpc_srv, ss)

	log.Println("Listen on :50054")
	if err := grpc_srv.Serve(lis); err != nil {
		log.Fatalf("Failed to serve: %v", err)
	}

}
