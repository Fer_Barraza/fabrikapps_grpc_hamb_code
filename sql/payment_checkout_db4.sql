-- MySQL dump 10.13  Distrib 8.0.23, for Linux (x86_64)
--
-- Host: localhost    Database: payment_checkout_db4
-- ------------------------------------------------------
-- Server version	8.0.23-0ubuntu0.20.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Branch`
--

DROP TABLE IF EXISTS `Branch`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Branch` (
  `Branch_Id` mediumint unsigned NOT NULL AUTO_INCREMENT,
  `Branch_Name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`Branch_Id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Branch`
--

LOCK TABLES `Branch` WRITE;
/*!40000 ALTER TABLE `Branch` DISABLE KEYS */;
INSERT INTO `Branch` VALUES (1,'Fabrikapps'),(2,'ECorp');
/*!40000 ALTER TABLE `Branch` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Branch_has_Headquarters`
--

DROP TABLE IF EXISTS `Branch_has_Headquarters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Branch_has_Headquarters` (
  `Branch_Branch_Id` mediumint unsigned NOT NULL,
  `Headquarters_Headquarters_Id` mediumint unsigned NOT NULL,
  PRIMARY KEY (`Branch_Branch_Id`,`Headquarters_Headquarters_Id`),
  KEY `fk_Branch_has_Headquarters_Headquarters1_idx` (`Headquarters_Headquarters_Id`),
  KEY `fk_Branch_has_Headquarters_Branch1_idx` (`Branch_Branch_Id`),
  CONSTRAINT `fk_Branch_has_Headquarters_Branch1` FOREIGN KEY (`Branch_Branch_Id`) REFERENCES `Branch` (`Branch_Id`),
  CONSTRAINT `fk_Branch_has_Headquarters_Headquarters1` FOREIGN KEY (`Headquarters_Headquarters_Id`) REFERENCES `Headquarters` (`Headquarters_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Branch_has_Headquarters`
--

LOCK TABLES `Branch_has_Headquarters` WRITE;
/*!40000 ALTER TABLE `Branch_has_Headquarters` DISABLE KEYS */;
INSERT INTO `Branch_has_Headquarters` VALUES (1,1),(1,2),(2,3);
/*!40000 ALTER TABLE `Branch_has_Headquarters` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Category`
--

DROP TABLE IF EXISTS `Category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Category` (
  `CategoriasMenu_id` int unsigned NOT NULL AUTO_INCREMENT,
  `CategoriasMenuNombre` varchar(25) NOT NULL,
  `Categorias_UrlImage` varchar(120) DEFAULT NULL,
  PRIMARY KEY (`CategoriasMenu_id`),
  UNIQUE KEY `CategoriasMenuNombre_UNIQUE` (`CategoriasMenuNombre`)
) ENGINE=InnoDB AUTO_INCREMENT=106 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Category`
--

LOCK TABLES `Category` WRITE;
/*!40000 ALTER TABLE `Category` DISABLE KEYS */;
INSERT INTO `Category` VALUES (1,'Hamburguesas','https://i.pinimg.com/originals/f6/5c/fc/f65cfc8f85c459ac731f6d72539ba8e4.jpg'),(2,'Papas','https://www.jocooks.com/wp-content/uploads/2015/01/loaded-pub-fries-8.jpg'),(3,'Cervezas','https://www.allagash.com/wp-content/uploads/Cascara-stories-3-scaled.jpg'),(4,'Pizzas','https://notife.com/wp-content/uploads/2021/02/pizza-ccc.jpg'),(104,'Bebidas','https://d1a7biwwgyx8zb.cloudfront.net/thumbs/1200x900c/notas/thumb/gaseosas520.jpg');
/*!40000 ALTER TABLE `Category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Delivery_Address`
--

DROP TABLE IF EXISTS `Delivery_Address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Delivery_Address` (
  `Delivery_Address_Id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `Delivery_Address_Street_Name` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'n/a',
  `Delivery_Address_Street_Number` mediumint unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`Delivery_Address_Id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Delivery_Address`
--

LOCK TABLES `Delivery_Address` WRITE;
/*!40000 ALTER TABLE `Delivery_Address` DISABLE KEYS */;
INSERT INTO `Delivery_Address` VALUES (0,'n/a',0),(1,'Lejos',666),(2,'Guale',2424),(3,'wasssa',4464);
/*!40000 ALTER TABLE `Delivery_Address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Headquarters`
--

DROP TABLE IF EXISTS `Headquarters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Headquarters` (
  `Headquarters_Id` mediumint unsigned NOT NULL AUTO_INCREMENT,
  `Headquarters_Name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`Headquarters_Id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Headquarters`
--

LOCK TABLES `Headquarters` WRITE;
/*!40000 ALTER TABLE `Headquarters` DISABLE KEYS */;
INSERT INTO `Headquarters` VALUES (1,'Sede MG'),(2,'Sede SJ'),(3,'Sede New York');
/*!40000 ALTER TABLE `Headquarters` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Item`
--

DROP TABLE IF EXISTS `Item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Item` (
  `Item_Id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `Item_Title` varchar(45) NOT NULL,
  `Item_Currency_id` varchar(5) DEFAULT 'ARS',
  `Item_Unit_Price` float NOT NULL,
  `Item_CategoriasMenu_id` int unsigned NOT NULL,
  `Item_Description` varchar(45) DEFAULT NULL,
  `Item_ImageName` varchar(90) DEFAULT NULL,
  `Branch_has_Headquarters_Branch_Branch_Id` mediumint unsigned NOT NULL,
  `Branch_has_Headquarters_Headquarters_Headquarters_Id` mediumint unsigned NOT NULL,
  PRIMARY KEY (`Item_Id`,`Branch_has_Headquarters_Branch_Branch_Id`,`Branch_has_Headquarters_Headquarters_Headquarters_Id`),
  KEY `fk_Item_Category1_idx` (`Item_CategoriasMenu_id`),
  KEY `fk_Item_Branch_has_Headquarters1_idx` (`Branch_has_Headquarters_Branch_Branch_Id`,`Branch_has_Headquarters_Headquarters_Headquarters_Id`),
  CONSTRAINT `fk_Item_Branch_has_Headquarters1` FOREIGN KEY (`Branch_has_Headquarters_Branch_Branch_Id`, `Branch_has_Headquarters_Headquarters_Headquarters_Id`) REFERENCES `Branch_has_Headquarters` (`Branch_Branch_Id`, `Headquarters_Headquarters_Id`),
  CONSTRAINT `fk_Item_Category1` FOREIGN KEY (`Item_CategoriasMenu_id`) REFERENCES `Category` (`CategoriasMenu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Item`
--

LOCK TABLES `Item` WRITE;
/*!40000 ALTER TABLE `Item` DISABLE KEYS */;
INSERT INTO `Item` VALUES (1,'Matadora','ARS',260,1,'Lechuga y tomate','hamburguesa_1.jpg',1,1),(2,'Poderosa','ARS',300,1,'Pepino','Mexicana.jpg',1,1),(101,'La Combinada','ARS',120,2,'Cheddar y bacon','fries_01.jpg',1,1),(102,'Turbulencia','ARS',150,2,'Cheddar, bacon y aderezos','muzzarelitas.jpg',1,1),(200,'Black','ARS',220,3,'Black','smoke-beer.jpg',1,1),(201,'Honey','ARS',200,3,'Honey','honey-beer.jpg',1,1),(202,'Irish','ARS',220,3,'Irish','rebel-red-ale.jpg',1,1),(250,'Muzarella','ARS',350,4,'Muzarella','pizza_casera_31391_600_square.jpg',1,1);
/*!40000 ALTER TABLE `Item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Item_has_Order_Detail`
--

DROP TABLE IF EXISTS `Item_has_Order_Detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Item_has_Order_Detail` (
  `Item_has_Order_Detail` bigint unsigned NOT NULL AUTO_INCREMENT,
  `Item_Item_Id` bigint unsigned NOT NULL,
  `Order_Order_Id` bigint unsigned NOT NULL,
  `Item_has_Order_Detail_Quantity` mediumint unsigned NOT NULL,
  `Item_has_Order_Detail_Unit_Price` float NOT NULL,
  PRIMARY KEY (`Item_has_Order_Detail`),
  KEY `fk_Item_has_Order_Item1_idx` (`Item_Item_Id`),
  KEY `fk_Item_has_Order_Order1_idx` (`Order_Order_Id`),
  CONSTRAINT `fk_Item_has_Order_Item1` FOREIGN KEY (`Item_Item_Id`) REFERENCES `Item` (`Item_Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_Item_has_Order_Order1` FOREIGN KEY (`Order_Order_Id`) REFERENCES `Orders` (`Order_Id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=263 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Item_has_Order_Detail`
--

LOCK TABLES `Item_has_Order_Detail` WRITE;
/*!40000 ALTER TABLE `Item_has_Order_Detail` DISABLE KEYS */;
INSERT INTO `Item_has_Order_Detail` VALUES (1,1,1,2,13131),(238,1,166,1,344),(241,1,167,1,111.1),(242,1,168,1,111.1),(243,1,169,1,111.1),(244,1,170,1,111.1),(245,1,171,1,111.1),(246,1,172,1,111.1),(247,1,173,1,111.1),(248,1,174,1,111.1),(249,1,175,1,111.1),(250,1,176,1,111.1),(251,1,177,1,111.1),(252,1,178,1,111.1),(253,1,179,1,111.1),(254,1,180,1,111.1),(255,1,181,1,111.1),(256,2,181,2,111.1),(257,1,182,1,111.1),(258,2,182,2,111.1),(259,1,183,1,111.1),(260,2,183,2,111.1),(261,1,184,1,111.1),(262,2,184,2,111.1);
/*!40000 ALTER TABLE `Item_has_Order_Detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Order_Status`
--

DROP TABLE IF EXISTS `Order_Status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Order_Status` (
  `Order_Status_Id` smallint NOT NULL AUTO_INCREMENT,
  `Order_Status_Type` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`Order_Status_Id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Order_Status`
--

LOCK TABLES `Order_Status` WRITE;
/*!40000 ALTER TABLE `Order_Status` DISABLE KEYS */;
INSERT INTO `Order_Status` VALUES (1,'--'),(2,'En proceso'),(3,'Entregado'),(4,'Cancelado'),(5,'En procese de devolución'),(6,'En camino'),(7,'Hubo un error grave'),(8,'Hubo un error en los datos del pedido');
/*!40000 ALTER TABLE `Order_Status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Orders`
--

DROP TABLE IF EXISTS `Orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Orders` (
  `Order_Id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `Order_PayerId` bigint unsigned NOT NULL,
  `Order_Status_Order_Status_Id` smallint NOT NULL DEFAULT '2',
  `Order_Method_Of_Delivery` varchar(25) NOT NULL,
  `Order_Payment_Method_Opts` varchar(45) NOT NULL,
  `Order_Payment_Total_Price` float unsigned NOT NULL,
  `Order_Date` date NOT NULL,
  `Order_Time` time NOT NULL,
  `Order_CodeMP` int unsigned DEFAULT '0',
  `Order_Branch_Id` mediumint unsigned NOT NULL,
  `Order_Headquarters_Id` mediumint unsigned NOT NULL,
  `Order_Address_Id` bigint unsigned NOT NULL,
  PRIMARY KEY (`Order_Id`,`Order_Address_Id`),
  KEY `fk_Order_Payer1_idx` (`Order_PayerId`),
  KEY `fk_Order_Status_idx` (`Order_Status_Order_Status_Id`),
  KEY `fk_Orders_Address1_idx` (`Order_Address_Id`),
  CONSTRAINT `fk_Order_Payer` FOREIGN KEY (`Order_PayerId`) REFERENCES `Payer` (`Payer_Id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `fk_Order_Status` FOREIGN KEY (`Order_Status_Order_Status_Id`) REFERENCES `Order_Status` (`Order_Status_Id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `fk_Orders_Address1` FOREIGN KEY (`Order_Address_Id`) REFERENCES `Delivery_Address` (`Delivery_Address_Id`)
) ENGINE=InnoDB AUTO_INCREMENT=185 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Orders`
--

LOCK TABLES `Orders` WRITE;
/*!40000 ALTER TABLE `Orders` DISABLE KEYS */;
INSERT INTO `Orders` VALUES (1,1,4,'Delivery','Efectivo',342332,'2021-03-20','00:00:00',0,1,1,1),(166,11,2,'Delivery','Efectivo',343.45,'2021-03-20','13:48:49',0,1,1,2),(167,124434222,2,'Delivery','Efectivo',111.1,'2021-03-20','18:38:42',0,1,1,3),(168,124434222,2,'Retiro en sucursal','Efectivo',111.1,'2021-03-20','18:39:15',0,1,1,0),(169,124434222,2,'Retiro en sucursal','Tarjeta',111.1,'2021-03-20','18:42:59',33556,1,1,0),(170,124434222,2,'Retiro en sucursal','Tarjeta',111.1,'2021-03-20','18:43:07',33556,1,1,0),(171,124434222,2,'Retiro en sucursal','Tarjeta',111.1,'2021-03-20','18:43:10',335536,1,1,0),(172,124434222,2,'Retiro en sucursal','Tarjeta',111.1,'2021-03-20','18:43:12',3355356,1,1,0),(173,124434222,2,'Retiro en sucursal','Tarjeta',111.1,'2021-03-20','18:43:15',33552356,1,1,0),(174,124434222,2,'Retiro en sucursal','Tarjeta',111.1,'2021-03-20','18:51:52',184467,1,1,0),(175,124434222,2,'Retiro en sucursal','Tarjeta',111.1,'2021-03-20','18:51:56',547983268,1,1,0),(176,124434222,2,'Retiro en sucursal','Tarjeta',111.1,'2021-03-20','18:51:59',184467,1,1,0),(177,124434222,2,'Retiro en sucursal','Tarjeta',111.1,'2021-03-20','18:52:02',93734276,1,1,0),(178,124434222,2,'Retiro en sucursal','Tarjeta',111.1,'2021-03-20','18:52:05',685440,1,1,0),(179,124434222,2,'Retiro en sucursal','Tarjeta',111.1,'2021-03-20','18:52:07',50688,1,1,0),(180,124434222,2,'Retiro en sucursal','Tarjeta',111.1,'2021-03-20','18:52:10',40832,1,1,0),(181,124434222,2,'Retiro en sucursal','Tarjeta',111.1,'2021-03-22','14:08:09',3355555556,1,1,0),(182,124434222,2,'Retiro en sucursal','Tarjeta',111.1,'2021-03-22','14:08:14',3490784497,1,1,0),(183,124434222,2,'Retiro en sucursal','Tarjeta',111.1,'2021-03-22','14:08:17',548106611,1,1,0),(184,124434222,2,'Retiro en sucursal','Tarjeta',111.1,'2021-03-22','14:08:19',1186497523,1,1,0);
/*!40000 ALTER TABLE `Orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Payer`
--

DROP TABLE IF EXISTS `Payer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Payer` (
  `Payer_Id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `Pay_Email` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `Pay_Name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `Pay_Surname` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `Pay_Phone_Number` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`Payer_Id`)
) ENGINE=InnoDB AUTO_INCREMENT=11435644964426249 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Payer`
--

LOCK TABLES `Payer` WRITE;
/*!40000 ALTER TABLE `Payer` DISABLE KEYS */;
INSERT INTO `Payer` VALUES (1,'fer@bar.com','fer','bar','1'),(11,'ela@hmail.ocm','ela','don','11'),(124434222,'Hello@fga','loco','lindo','124434222');
/*!40000 ALTER TABLE `Payer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Url_images`
--

DROP TABLE IF EXISTS `Url_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Url_images` (
  `Url_images` bigint unsigned NOT NULL AUTO_INCREMENT,
  `Url_images_Location` varchar(25) NOT NULL,
  `Item_Item_Id` bigint unsigned NOT NULL,
  PRIMARY KEY (`Url_images`),
  KEY `fk_Url_images_Item1_idx` (`Item_Item_Id`),
  CONSTRAINT `fk_Url_images_Item1` FOREIGN KEY (`Item_Item_Id`) REFERENCES `Item` (`Item_Id`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Url_images`
--

LOCK TABLES `Url_images` WRITE;
/*!40000 ALTER TABLE `Url_images` DISABLE KEYS */;
/*!40000 ALTER TABLE `Url_images` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-04-08 10:17:19
